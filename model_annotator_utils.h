/*
 *  model_annotator_utils.h.h
 *
 *  Created on: 14-Feb-2016
 *      Author: divya
 */

#ifndef MODEL_ANNOTATOR_UTILS_H_
#define MODEL_ANNOTATOR_UTILS_H_

#include "utils.h"
#include "model_common_utils.h"

void run_annotator_model(string settings_file, string output_file);
void run_annotator_model_test(string settings_file, string output_file);

#endif /*MODEL_ANNOTATOR_UTILS_H_*/
