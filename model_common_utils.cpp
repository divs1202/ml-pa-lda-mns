/*
 * model_common_utils.cpp
 *
 *  Created on: 14-Feb-2016
 *      Author: divya
 */
#include "model_common_utils.h"
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>
#include <boost/algorithm/string.hpp>
#include <math.h>



void initialize_suff(int total_classes, int total_topics, int vocab, int total_ann/*=0*/){
	for(int i=0; i<total_classes; i++)
		suff1[i] = 0;
	for(int j=0; j<total_ann; j++)
	{
		suff2[j] = 0;
		suff3[j] = 0;
	}
	for (int t=0; t<total_topics; t++){
		for (int v=0; v<vocab; v++)
			suff4[t][v] = 0;
	}

	for (int i=0 ; i <total_classes; i++){
		for (int t=0; t<total_topics; t++){
			suff5[i][0][t] =0 ;
			suff5[i][1][t] =0 ;
		}
	}
}
void create_and_initialize_suff(int total_classes, int total_topics, int vocab, int total_ann/*=0*/){
	suff1 = new double[total_classes];
	suff2 = new double[total_ann];
	suff3 = new int[total_ann];
	suff4= new double*[total_topics];
	for (int t=0; t<total_topics; t++)
		suff4[t] = new double[vocab];
	suff5 = new double**[total_classes];
	for (int i=0 ; i <total_classes; i++){
		suff5[i] = new double*[2];
		suff5[i][0] = new double[total_topics];
		suff5[i][1] = new double[total_topics];

	}
	initialize_suff(total_classes, total_topics, vocab, total_ann);
}
void delete_suff(int total_classes, int total_topics, int vocab, int total_ann/*=0*/){
	delete[] suff1;
	delete[] suff2;
	delete[] suff3;
	for (int t=0; t<total_topics; t++)
		delete[] suff4[t];
	delete[] suff4;
	for (int i=0 ; i <total_classes; i++){
		delete[] suff5[i][0];
		delete[] suff5[i][1];
	}
	delete[] suff5;

}



void read_svm_file_to_get_corpus(corpus *m_corpus,string filepath,settings settings, double *bins)
{
	fstream trainFile;
	trainFile.open(filepath.c_str(),fstream::in);
	if (!trainFile.good()){
			cout  << " The file "<< filepath << " was not found! Please "
					"correct the settings file. "<< endl;
			exit(0);
	}
	int flag = -1; //at start, 0 ---> current variables valid

	std::vector<short int> currDoc;
	int countBins[settings.svm_vocab_size];
	memset(countBins, 0, sizeof(countBins));
	int docid=1;
	int classes[settings.svm_classes];
	memset(classes, 0, sizeof(classes));

	int currentIndex = 0;
	double currentVal;
	while(!trainFile.eof())
	{
		string line;
		trainFile >> line;

		bool isNumber = true;
		for(string::const_iterator k = line.begin(); k != line.end(); ++k)
			isNumber = isNumber && isdigit(*k);
		if(line.find(",")!=string::npos || isNumber)
		{
			if(flag==0)
			{
				Doc *doc = new Doc(docid, settings.svm_classes, TOTAL_ANN, TOTAL_TOPICS, currDoc, classes );
				m_corpus->all_docs[docid]= doc;
				//cout<<"created docid:"<<docid<<endl;
				docid++;
				//create doc to corpus
			}
			currDoc.clear();
			memset(classes, 0, sizeof(classes));
			if(isNumber)
			{
				if(line.size()==0)
					break;
				int currclass = stoi(line);
				classes[currclass]=1;
				//cout<<line<<endl;
			}
			else
			{
				int currclass;
				vector<std::string> strs;
				//cout<<line<<endl;
				boost::split(strs, line, boost::is_any_of(","));
				for(vector<std::string>::iterator it= strs.begin(); it != strs.end();++it )
				{
					currclass = stoi(*it);
					classes[currclass]=1;
				}
			}
			flag=0;
		}
		else if(line.find(":")!=string::npos)
		{
			double tempdiff;
			int nearestBin = 0;
			vector<std::string> strs;
			boost::split(strs, line, boost::is_any_of(":"));

			//cout<<strs[1]<<endl;
			//cout<< strs[0] <<" : "<<strs[1]<<endl;
			currentIndex = stoi(strs[0]);
			currentVal = stod(strs[1]);
			if (settings.svm_DirectWords==0)
			{
				tempdiff=fabs(bins[nearestBin]-currentVal);

				for(int i=1; i<settings.svm_vocab_size; ++i)
				{
					double currentdiff = fabs(bins[i]-currentVal);
					if(tempdiff>currentdiff)
					{
						nearestBin =i;
						tempdiff=fabs(bins[nearestBin]-currentVal);
					}
					else
					{
						//					if (tempdiff > 0.001 )
						//						cout<<tempdiff<<endl;
						//cout<<nearestBin<<endl;
						break;
					}
				}
				for(int i = currDoc.size()+1; i<currentIndex; i++)
					currDoc.push_back(0);
				currDoc.push_back(nearestBin);
				countBins[nearestBin]++;
			}
			else
			{
				int pushCount = 0;
				while(pushCount !=currentVal)
				{
					currDoc.push_back(currentIndex);				
					pushCount++;
				}				
			}
			//cout<<currentVal<<endl;
		}
		else
		{
			cout<<"Invalid line "<<line<<endl;
			exit(0);
		}
	}
	//	for(int i=0;i<settings.vocab_size;i++)
	//		cout<<countBins[i]<<endl;
	trainFile.close();
}


template <typename T>
void print_vector(vector<T> arg){
	int count  = 1;
	for (typename vector<T>::iterator it =  arg.begin(); it != arg.end(); it++, ++count)
		cout << count<<". "<< *it<<endl;
	return;
}

void set_global_constants(string training_dir, string dir, string vocab_file/*="final_vocab.txt"*/, string class_file/*="final_class.txt"*/){
	set<string> vocab_set = populate_word_list (dir+vocab_file);
	vocab_list.insert(vocab_list.begin(),vocab_set.begin(), vocab_set.end());
	set<string> class_set = populate_word_list(dir+class_file);
	class_list.insert(class_list.begin(),class_set.begin(), class_set.end());
	vector<string> doc_list = read_directory(training_dir.c_str());//"/home/divya/workspace/multiLabelCrowd/src/phase6/");
	VOCAB = vocab_set.size(); TOTAL_CLASSES = class_set.size();
	TOTAL_DOCS = doc_list.size(); TOTAL_TOPICS = TOTAL_CLASSES*2;
}

void print_global_constants(){
	cout << " VOCAB = " << VOCAB<<endl;
	cout << " TOTAL_CLASSES = "<< TOTAL_CLASSES<< endl;
	cout << " TOTAL_DOCS = "<< TOTAL_DOCS<<endl;
	cout << " TOTAL TOPICS = "<< TOTAL_TOPICS<<endl;
	cout << " TOTAL ANN = "<<TOTAL_ANN<<endl;
}

void reset_params(int m_total_topics, int m_total_classes,int num_words,
		double *&c_delta,//[TOTAL_CLASSES];
		double ***&m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **&phi,//[num_words][TOTAL_TOPICS];
		double **&delta//[num_words][TOTAL_CLASSES];
){
	long seed = time(NULL);
	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r, seed);

	c_delta = new double[m_total_classes];
	m_gamma = new double**[m_total_classes];
	for(int i=0; i<m_total_classes; ++i)
	{
		c_delta[i] = gsl_ran_beta(r, 10,10);
		m_gamma[i] = new double*[2];
		m_gamma[i][0] = new double[m_total_topics];
		m_gamma[i][1] = new double[m_total_topics];
		for (int t=0; t< m_total_topics; t++){
			//cout<<" Accessing " << doc_id<<" gamma "<<endl;
			m_gamma[i][0][t]= gsl_ran_beta(r, 1,1);
			m_gamma[i][1][t]= gsl_ran_beta(r, 1,1);
			//cout<<" Done with accessing " << doc_id<<" gamma "<<endl;
			/*	m_gamma[i][0][t]= 1.0;
               	m_gamma[i][1][t]= 100.0;*/

		}

	}

	phi= new double*[num_words];
	//    cout <<" Resetting phi to size "<< num_words << ", "<< m_total_topics;


	double* dir_param = new double[m_total_topics];
	for (int i=0; i< m_total_topics; i++)
		dir_param[i] = 1.0;//100.0;

	for(int i=0; i<num_words; ++i)
	{
		phi[i] = new double[m_total_topics];
		gsl_ran_dirichlet(r, m_total_topics, dir_param, phi[i]);
	}
	delete[] dir_param;

	delta = new double*[num_words];

	for(int n=0; n<num_words; ++n)
	{
		delta[n] = new double[m_total_classes];
		// gsl_ran_dirichlet(r, m_total_topics, dir_param, delta[n]);
		for (int i=0; i<m_total_classes; i++)
			delta[n][i] = 1.0/m_total_classes;
	}
	gsl_rng_free(r);
}




void delete_params(int m_total_topics, int m_total_classes,int num_words,
		double *&c_delta,//[TOTAL_CLASSES];
		double ***&m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **&phi,//[num_words][TOTAL_TOPICS];
		double **&delta//[num_words][TOTAL_CLASSES];
){
	for(int i=0; i<m_total_classes; ++i)
	{
		delete[] m_gamma[i][0];
		delete[] m_gamma[i][1];
		delete[] m_gamma[i];
	}
	delete[] c_delta;
	delete[] m_gamma;
	for (int i=0; i<num_words; ++i)
	{
		delete[] phi[i];
		delete[] delta[i];
	}
	delete[] phi;
	delete[] delta;

}


Doc* read_doc_from_file(string dirname, string filename){
	// string filename = "9.txt";
	//    string dirname = "/home/divya/workspace/multiLabelCrowd/src/phase6/";
	string filepath = dirname; filepath.append(filename);
	fstream doc_input;
	doc_input.open(filepath.c_str(), fstream::in);
	short int doc_id;
	sscanf(filename.c_str(), "%hd", &doc_id);
	//cout << "Id = "<< doc_id << endl;
	int doc_start = 0;
	int class_vec_doc[TOTAL_CLASSES];
	vector<short int> word_list_doc;
	for (int i=0; i< TOTAL_CLASSES; i++)
		class_vec_doc[i]=0;
	int doc_include_flag =1;
	while(! doc_input.eof()){
		string line;
		doc_input>>line;
		if(line.size()==0)
			continue;
		if (strcmp(line.c_str(), "*****") == 0) {
			doc_start = 1;
			continue;
		}
		if (doc_start == 0)
		{
			if  (find(class_list.begin(), class_list.end(), line)== class_list.end())
				doc_include_flag = 0;
			else
				doc_include_flag = 1;
			if (doc_include_flag == 1){
				int index = find(class_list.begin(), class_list.end(), line) - class_list.begin();
				/* To be commented later on */
				/*if (index == 9)
				doc_include_flag = 0;*/

				/* To be commented till here later */
				/* To be commented later on */
				/*if ((index == 2) || (index == 3) || (index == 5) || (index==6))
				index = 2;
			else{
				if ((index ==4) || (index == 9))
					index = 3;
				else{
					if ((index == 7) ||(index == 8) )
						index = 4;
				}
			}*/
				/* To be commented till here later */
				class_vec_doc[index]=1;
			}
		}
		if (doc_start == 1 && doc_include_flag == 1){
			int index = find(vocab_list.begin(), vocab_list.end(), line) - vocab_list.begin();
			word_list_doc.push_back(index);
		}
	}

	doc_input.close();
	/*	cout << " Class vector = ";
		for (int i=0; i< TOTAL_CLASSES; i++)
			cout<< class_vec_doc[i];
		cout << " Word vector :" << endl;
		for (int i=0; i< word_list_doc.size(); i++)
		{
			cout<<i<<". "<< word_list_doc[i] <<" \t "<< vocab_list[word_list_doc[i]] <<endl;
		}
		// created a doc
		cout<<" ********************************* ";*/
	Doc *m_doc = NULL;
	if (doc_include_flag == 1)
		m_doc = new Doc(doc_id, TOTAL_CLASSES, TOTAL_ANN, TOTAL_TOPICS,
				word_list_doc, class_vec_doc);

	/*	cout<< " Created doc "<< doc_id<<" successfully!" << endl;
		m_doc.print_doc_contents();*/
	return m_doc;
}

map<short int, Doc*> form_docid_doc_map(string dir_name){
	map<short int, Doc*> doc_set;
	vector<string> file_list = read_directory(dir_name);
	for ( int i=0; i< file_list.size(); i++){
		Doc* m_doc = read_doc_from_file(dir_name,file_list[i]);
		if (m_doc!=NULL)
		{
			doc_set[m_doc->doc_id] = m_doc;// int class_seen_flag = 0; int unique_class =0;			
		}
	}
	return doc_set;
}
void print_settings(settings m_settings){
	cout	<<" param_file "<<m_settings.param_file<<endl
			<<" test_res_folder "<<m_settings.test_res_folder<<endl
			<<" output_file "<< m_settings.output_file << endl
			<<" num_ann "<<m_settings.num_ann<<endl
			<<" output_src"<<m_settings.outer_src;
}
settings read_from_settings_file(string settings_file){
	fstream file_h;
	file_h.open(settings_file.c_str(), fstream::in);
	struct flags_read{
		int num_ann_flag = 0, model_folder_flag =0,
				param_file_flag = 0, test_res_flag = 0,
				output_file_flag = 0, outer_src_flag = 0, ann_folder_flag=0,
				dataset_name_flag=0;
		int svm_trng_src_flag=1;
		int svm_test_src_flag=1;;
		int svm_vocab_flag=1;
		int svm_delim_flag=1;
		int svm_classes_flag=1;
		int svm_topics=1;
		int svm_direct_words_flag=1;

	}flags;
	memset(&flags, 0, sizeof(flags));

	string vocab_file, class_file, trng_src , test_src, model_folder, param_file,
	test_res_folder, output_file , outer_src;
	int num_topics, num_ann, to_train=0;
	int counter = 0;
	settings input_settings;
	while(!file_h.eof()){
		counter++;
		string line; file_h >> line;
		if (line.empty())
			break;
		//cout<< counter<<". "<< line << endl;
		if (counter%2 == 1){
			if(line.compare("num_ann")==0)
			{
				memset(&flags, 0, sizeof(flags));
				flags.num_ann_flag = 1;
				continue;
			}
			if(line.compare("model_folder")==0)
			{
				memset(&flags, 0, sizeof(flags));
				flags.model_folder_flag =1;
				continue;
			}
			if(line.compare("param_file")==0)
			{
				memset(&flags, 0, sizeof(flags));
				flags.param_file_flag = 1;
				continue;
			}
			if(line.compare("test_result_folder")==0)
			{
				memset(&flags, 0, sizeof(flags));
				flags.test_res_flag = 1;
				continue;
			}
			if(line.compare("output_file")==0)
			{
				memset(&flags, 0, sizeof(flags));
				flags.output_file_flag = 1;
				continue;
			}
			if(line.compare("outer_src")==0)
			{
				memset(&flags, 0, sizeof(flags));
				flags.outer_src_flag = 1;
				continue;
			}
			if(line.compare("svm_trng")==0)
			{
				memset(&flags, 0, sizeof(flags));
				flags.svm_trng_src_flag =1;
				continue;
			}
			if(line.compare("svm_test")==0)
			{
				memset(&flags, 0, sizeof(flags));
				flags.svm_test_src_flag =1;
				continue;
			}
			if(line.compare("svm_vocab")==0)
			{
				memset(&flags, 0, sizeof(flags));
				flags.svm_vocab_flag =1;
				continue;
			}
			if(line.compare("svm_split")==0)
			{
				memset(&flags, 0, sizeof(flags));
				flags.svm_delim_flag =1;
				continue;
			}
			if(line.compare("svm_classes")==0)
			{
				memset(&flags,0,sizeof(flags));
				flags.svm_classes_flag = 1;
				continue;
			}
			if(line.compare("svm_topics")==0)
			{
				memset(&flags,0,sizeof(flags));
				flags.svm_topics=1;
				continue;
			}
			if(line.compare("ann_folder")==0)
			{
				memset(&flags,0,sizeof(flags));
				flags.ann_folder_flag = 1;
				continue;
			}
			if(line.compare("dataset_name")==0){
				memset(&flags,0,sizeof(flags));
				flags.dataset_name_flag = 1;
				continue;
			}
			if(line.compare("svm_direct_words")==0)
			{
				memset(&flags,0,sizeof(flags));
				flags.svm_direct_words_flag=1;
				continue;
			}
		}
		else{if(flags.num_ann_flag==1)
			{
				input_settings.num_ann = stold(line);
				continue;
			}
			if(flags.model_folder_flag==1)
			{
				input_settings.model_folder = line;
				continue;
			}
			if(flags.param_file_flag==1)
			{
				input_settings.param_file = line;
				continue;
			}
			if(flags.test_res_flag==1)
			{
				input_settings.test_res_folder = line;continue;
			}
			if(flags.output_file_flag==1)
			{
				input_settings.output_file = line;continue;
			}
			if(flags.outer_src_flag==1)
			{
				input_settings.outer_src = line;continue;
			}
			if(flags.svm_trng_src_flag==1)
			{
				input_settings.svm_trng_src= line;
				continue;
			}
			if(flags.svm_test_src_flag==1)
			{
				input_settings.svm_test_src = line;
				continue;
			}
			if(flags.svm_vocab_flag==1)
			{
				//cout<<line;
				input_settings.svm_vocab_size = stoi(line);
				continue;
			}
			if(flags.svm_delim_flag==1)
			{
				input_settings.svm_delim_file = line;
				continue;
			}
			if(flags.svm_classes_flag==1)
			{
				input_settings.svm_classes = stoi(line);
				continue;
			}
			if(flags.svm_topics==1)
			{
				input_settings.svm_topics=stoi(line);
				continue;
			}
			if(flags.ann_folder_flag == 1)
			{
				input_settings.ann_folder = line;continue;
			}
			if(flags.dataset_name_flag == 1)
			{
				input_settings.dataset_name = line;continue;
			}
			if(flags.svm_direct_words_flag==1)
			{
				input_settings.svm_DirectWords=stoi(line);
				continue;
			}
		}

	}
	file_h.close();
	//print_settings(input_settings);
	return input_settings;
}

gen_model_params* read_param_file(string filename){
//	cout << "TOTAL ANN = "<< TOTAL_ANN<<endl;
//	cout << "TOTAL CLASSES = "<< TOTAL_CLASSES<<endl;
//	cout << "TOTAL TOPICS = "<< TOTAL_TOPICS<<endl;
//	cout << "TOTAL VOCAB = "<< VOCAB<<endl;
	gen_model_params* m_gen_param = new gen_model_params(TOTAL_CLASSES, TOTAL_TOPICS, VOCAB, TOTAL_ANN);
	fstream file_h;
	file_h.open(filename.c_str(), fstream::in);
	if (!file_h.good()){
		cout << " The model file "<< filename <<" does not exist. Please check your input "<<endl;
		exit(0);
	}
	int line_count = 0; int xi_flag=0, rho_flag=0, alpha_flag=0, eta_flag=0, kappa_flag = 0;
	int xi_index  = 0, rho_index = 0, alpha_i = 0, alpha_j = 0, alpha_t = 0, eta_t = 0 , eta_j = 0, kappa_index = 0;
	while (!file_h.eof()){
		string line; file_h >> line;
		//cout << line_count<< ". "<<line<<endl;
		if(line.compare("xi:")==0)
		{
			xi_flag = 1; rho_flag=0; alpha_flag=0; eta_flag=0; kappa_flag = 0;
			xi_index = 0;
			continue;
		}
		if(line.compare("rho:")==0)
		{
			xi_flag = 0; rho_flag=1; alpha_flag=0; eta_flag=0;kappa_flag = 0;
			rho_index = 0;
			continue;
		}
		if(line.compare("kappa:")==0)
		{
			xi_flag = 0; rho_flag=0; alpha_flag=0; eta_flag=0; kappa_flag = 1;
			kappa_index =0;
			continue;
		}
		if(line.compare("alpha:")==0)
		{
			xi_flag = 0; rho_flag=0; alpha_flag=1; eta_flag=0; kappa_flag = 0;
			alpha_i= 0; alpha_j = 0; alpha_t=0;
			continue;
		}
		if(line.compare("beta:")==0)
		{
			xi_flag = 0; rho_flag=0; alpha_flag=0; eta_flag=1;kappa_flag = 0;
			eta_t = 0; eta_j=0;
			//cout << " eta flag = 1";
			continue;
		}
		if (xi_flag == 1){
			m_gen_param->xi[xi_index] = stod(line); xi_index++;
			if (xi_index == TOTAL_CLASSES)
				xi_flag = 0;
			continue;
		}
		if ( rho_flag == 1){
			m_gen_param->rho[rho_index] = stod(line); rho_index++;
			if (rho_index == TOTAL_ANN)
				rho_flag = 0;
			continue;
		}
		if (alpha_flag == 1){
			//cout<<alpha_i<<" "<< alpha_j<<" "<< alpha_t<<" : "<<line<<endl;
			m_gen_param->alpha[alpha_i][alpha_j][alpha_t] = stold(line);
			alpha_t++;
			if (alpha_t == TOTAL_TOPICS){
				alpha_j++; alpha_t =0;
			}
			if (alpha_j==2){
				alpha_j = 0; alpha_i++;
			}
			if (alpha_i == TOTAL_CLASSES)
				alpha_flag =0;
			continue;
		}

		if (eta_flag == 1){
			m_gen_param->beta[eta_t][eta_j] = stold(line);
			eta_j++;
			if (eta_j == VOCAB){
				eta_j=0; eta_t++;
			}
			if(eta_t == TOTAL_TOPICS)
			{
				eta_flag= 0;
			}
			continue;
		}
		if (kappa_flag == 1){
			m_gen_param->kappa[kappa_index] = stold(line);
			kappa_index++;
			if (kappa_index == TOTAL_CLASSES)
				kappa_flag =0;
			continue;
		}
		line_count++;
	}
	file_h.close();
	return m_gen_param;
}
void findMinMax(int* idmin, int *idmax)
{
	*idmin=0; *idmax=0;
	for(int i=1; i<TOTAL_CLASSES; ++i)
	{
		if(class_size[*idmin] > class_size[i])
			*idmin=i;
		if(class_size[*idmax] < class_size[i])
			*idmax=i;
	}
}
Doc* addToCorpus(corpus *m_corpus, short int doc_id)
{
	// find the largest doc id
	short int largest_id = m_corpus->all_docs.begin()->first;
	for (map<short int, Doc*> ::iterator it = m_corpus->all_docs.begin();
			it != m_corpus->all_docs.end();
			it++){
		short int doc_id = it->first;
		if (doc_id> largest_id)
			largest_id = doc_id;
	}
	int new_doc_id =largest_id+1;
	//	cout<<  new_doc_id <<"  doc created!" << endl;
	Doc *doc = m_corpus->all_docs[doc_id];
	Doc *newDoc;
	if (doc->ann_present_flag == 1){
		newDoc= new Doc(new_doc_id, TOTAL_CLASSES, TOTAL_ANN, TOTAL_TOPICS,
				doc->docwords, doc->doc_classes, 0, doc->ann_classes);
	}
	else
		newDoc= new Doc(new_doc_id, TOTAL_CLASSES, TOTAL_ANN, TOTAL_TOPICS,
				doc->docwords, doc->doc_classes);


	m_corpus->all_docs[new_doc_id] = newDoc;
	m_corpus->docs_to_be_deleted.push_back(new_doc_id);
	return newDoc;
}


vector<double> get_score(string dirname, string outer_src, string file_name/*="results.txt"*/){
	vector<string> test_result_files = read_directory(dirname); double log_l = 0;
	string file_name_out = outer_src + file_name;
	vector<double> final_results;
	fstream file_h_out ; file_h_out.open(file_name_out.c_str(), fstream::out);
	file_h_out << " NUM_FILES \t "<< test_result_files.size()<< endl;
	int tp=0, tn=0, fp=0, fn=0;
	int tp_class[TOTAL_CLASSES], tn_class[TOTAL_CLASSES], fp_class[TOTAL_CLASSES], fn_class[TOTAL_CLASSES];
	double threshold[TOTAL_CLASSES];
	for (int i=0; i<TOTAL_CLASSES; i++)
	{
		threshold[i]= 0.5;
		file_h_out<<" threshold \t"<<i<<"\t " << threshold[i]<< endl;
		tp_class[i] = 0; tn_class[i] =0; fp_class[i] = 0; fn_class[i] = 0;
	}
	for (int file_it = 0; file_it< test_result_files.size(); file_it++){
		fstream file_h;
		string filename = dirname; filename.append(test_result_files[file_it]);
		file_h.open(filename.c_str(), fstream::in);
		int counter = 0; int index = 0; double prob; int true_val =0 , pred_val =0;
		while(!file_h.eof()){
			string line;
			file_h >> line;
			//cout << line<< endl;
			if (counter == 0){
				counter++; continue;
			}
			if (counter%2 == 1)
			{
				index = counter/2;
				true_val = stold(line);
			}
			else
			{
				prob= stold(line);
				pred_val = prob>threshold[index];
				if(pred_val ==1 && true_val==1)
				{
					tp++; tp_class[index] ++;
				}
				if (pred_val==0 && true_val == 0)
				{
					tn++; tn_class[index] ++;
				}
				if(pred_val==0 && true_val==1)
				{
					fn++; fn_class[index] ++;
				}
				if(pred_val==1 && true_val==0)
				{
					fp++; fp_class[index] ++;
				}
				log_l += (true_val*log(max(prob, 1e-6)) + (1-true_val)*log(max(1-prob, 1e-6)));

			}
			counter++;
			if (counter > TOTAL_CLASSES*2)
				break;

		}
		file_h.close();
		//break;
	}
	double accuracy = (tp + tn)*1.0/(tp+ tn+ fp+ fn);
	final_results.push_back(accuracy);
	double micro_p = tp*1.0/(tp+fp);
	double micro_r  = tp*1.0/(tp+fn);
	double micro_f = 2*micro_p*micro_r*1.0/(micro_p + micro_r);
	log_l = log_l*1.0/(test_result_files.size()*TOTAL_CLASSES);
	final_results.push_back(log_l);
	file_h_out << " Overall \n TP \t "<<tp<<"\n TN \t "<< tn<<"\n FP \t "<< fp<<"\n FN \t "<< fn<< endl;
	file_h_out << " Accuracy \t "<< accuracy <<" \n Log-l \t "<< log_l<<endl;
	file_h_out<<" Micro-f \t" << micro_f << endl;
	for (int i=0; i<TOTAL_CLASSES; i++){
		file_h_out<<" Class \t "<< i << endl;
		file_h_out << " TP \t "<<tp_class[i]<<"\n TN \t "<< tn_class[i]<<"\n FP \t  "<< fp_class[i]<<"\n FN \t  "<< fn_class[i]<< endl;
		file_h_out<<"  Accuracy \t "<< (tp_class[i]+ tn_class[i])*1.0
				/(tp_class[i]+ tn_class[i]+ fp_class[i]+ fn_class[i])<<endl;
	}
	file_h_out.close();
	final_results.push_back(micro_f);
	return final_results;
}

double log_likelihood_doc_test(Doc* doc, gen_model_params* m_gen,
		int m_topics,
		int m_classes,
		int num_words,
		double *&c_delta,//[TOTAL_CLASSES];
		double ***&m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **&phi,//[num_words][TOTAL_TOPICS];
		double **&delta//[num_words][TOTAL_CLASSES];
){
	double sum_gamma[TOTAL_CLASSES][2], digamma_f_gamma[TOTAL_CLASSES][2][TOTAL_TOPICS];
	double sum_alpha[TOTAL_CLASSES][2], digamma_f_alpha[TOTAL_CLASSES][2][TOTAL_TOPICS];
	//double sum_chi[TOTAL_TOPICS], digamma_f_chi[TOTAL_TOPICS][VOCAB];
	//double sum_eta[TOTAL_TOPICS], digamma_f_eta[TOTAL_TOPICS][VOCAB];

	// populate psi functions for gamma and alpha
	for (int i=0; i< TOTAL_CLASSES; i++){
		for (int j=0; j<=1; j++){
			sum_gamma[i][j] = 0; sum_alpha[i][j]=0;
			for (int t=0; t< TOTAL_TOPICS; t++){
				sum_gamma[i][j] += m_gamma[i][j][t];
				digamma_f_gamma[i][j][t] = gsl_sf_psi(m_gamma[i][j][t]);
				sum_alpha[i][j] += m_gen->alpha[i][j][t];
				digamma_f_alpha[i][j][t] = gsl_sf_psi(m_gen->alpha[i][j][t]);

			}

			for (int t=0; t<TOTAL_TOPICS; t++){
				digamma_f_gamma[i][j][t] -= gsl_sf_psi(sum_gamma[i][j]);
				digamma_f_alpha[i][j][t] -= gsl_sf_psi(sum_alpha[i][j]);
			}
		}
	}
	//cout << " Computed psi for gamma and alpha"<< endl;
	// populate psi functions for chi and eta

	//cout << " Computed psi for chi  and eta"<< endl;
	double elbo = 0;
	for (int i=0; i< TOTAL_CLASSES; i++){
		elbo += (c_delta[i]*(log(max(m_gen->xi[i],1e-6 )) -
				log(max(c_delta[i],1e-6))))+
						((1-c_delta[i])*
								(log(max(1-m_gen->xi[i],1e-6))-log(max(1-c_delta[i],1e-6)) ));
		for (int j=0; j<=1; j++){
			elbo+= gsl_sf_lngamma(sum_alpha[i][j]) - gsl_sf_lngamma(sum_gamma[i][j]);
			for (int t=0; t< TOTAL_TOPICS; t++){
				elbo = elbo + gsl_sf_lngamma(m_gamma[i][j][t])
														- gsl_sf_lngamma(m_gen->alpha[i][j][t]) +
														((m_gen->alpha[i][j][t] - m_gamma[i][j][t])*digamma_f_gamma[i][j][t]);

				for(int n=0; n< doc->num_words; n++){
					elbo+= pow(c_delta[i],j)*
							pow(1-c_delta[i], 1-j)*
							delta[n][i] * phi[n][t]* digamma_f_gamma[i][j][t];
				}
			}
		}
		//	cout << i<<". Finished i-j-t-n. Now out of j loop" << endl;
		for (int n=0; n<doc->num_words; n++){
			elbo += delta[n][i]*(log(m_gen->kappa[i])
					- log(max(delta[n][i],1e-6)));
		}


	}
	//cout << " Finished i-j-t-n. Now out of i loop" << endl;
	for (int t=0; t<TOTAL_TOPICS; t++){
		//cout << "sum_eta:" << sum_eta[t] <<" , sum_chi: "<< sum_chi[t]<<endl;
		//	elbo+= gsl_sf_lngamma(sum_eta[t]) - gsl_sf_lngamma(sum_chi[t]);
		//cout << " Entering j loop" << endl;
		for (int n=0; n<doc->num_words; n++){
			int j=doc->docwords[n];
			elbo+= (phi[n][t]* (log(max(m_gen->beta[t][j],1e-6))
					- log(max(phi[n][t],1e-6))));
		}
	}

	// account for annotators
	if (doc->ann_present_flag == 1){
		for (int j=0; j<TOTAL_ANN; j++){
			for (int i=0; i<TOTAL_CLASSES; i++){

				if (doc->ann_classes[j][i]!=-1){
					elbo += ((c_delta[i]*doc->ann_classes[j][i] )+
							(1-c_delta[i])*(1-doc->ann_classes[j][i]))*
									log(max(m_gen->rho[j],1e-6));
					elbo +=((c_delta[i]*(1-doc->ann_classes[j][i] ))+
							(1-c_delta[i])*(doc->ann_classes[j][i]))*
									log(max(1-m_gen->rho[j],1e-6));
				}
			}
		}
	}
	return elbo;
}



