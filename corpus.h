/*
 *  corpus.h
 *
 *  Created on: 14-feb-2016
 *      Author: divya
 */

#ifndef CORPUS_H_
#define CORPUS_H_
#include "utils.h"
#include "document.h"


class corpus{
public:
	map<short int,Doc*> all_docs;
	vector<Doc*>all_docs_vec;
	vector<short int> docs_to_be_deleted;
	int total_num_virtual_docs;
	double log_l;
	int mem_managed=1;
	corpus(int i=1)
	{
		mem_managed=i;
	}
	void reset_weights_of_docs(){
		for(map<short int,Doc*>::iterator it = all_docs.begin(); it!= all_docs.end(); it++)
		{
			Doc* m_doc = it->second;
			m_doc->weightage = 1;
		}
		return;
	}
	int compute_virtual_num_docs(){
		int total_virtual_docs = 0;
		for(map<short int,Doc*>::iterator it = all_docs.begin(); it!= all_docs.end(); it++)
		{
			Doc* m_doc = it->second;
			total_virtual_docs += m_doc->weightage;
		}
		total_num_virtual_docs = total_virtual_docs;
		return total_virtual_docs;
	}
	~corpus(){
		if (docs_to_be_deleted.size()>0)
		{
			for(int it =0; it<docs_to_be_deleted.size(); it++)
			{
				if (all_docs.find(docs_to_be_deleted[it])!=all_docs.end())
					delete all_docs[docs_to_be_deleted[it]];
			}
		}
		if(mem_managed==1)
			return;
		for(map<short int,Doc*>::iterator it = all_docs.begin(); it!= all_docs.end(); it++)
		{
			if(it->second!=NULL)
				delete it->second;
			it->second = NULL;
		}

	}
};

#endif /*CORPUS_H_*/

