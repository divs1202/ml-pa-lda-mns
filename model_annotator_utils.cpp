/*
 *  model_annotator_utils.cpp
 *
 *  Created on: 14-Feb-2016
 *      Author: divya
 */
#include "model_common_utils.h"
#include "model_annotator_utils.h"

// sufficient statistics
double *suff1; //sum of c_delta_i
double *suff2; // annotator terms sum
int *suff3; //no. of labels given by each annotator
double **suff4; //beta numerator term
double ***suff5; // digamma_sum term, used in alpha opt

int VOCAB;
int TOTAL_CLASSES;
int TOTAL_DOCS;
int TOTAL_TOPICS;
int TOTAL_ANN = 0;
int MAX_E_ITER_TEST = 100;
int MAX_EM_ITER = 150;
int MAX_NR_ITERATIONS = 100;
int MAX_E_ITER_TRNG = 100;

int VIRTUAL_TOTAL_DOCS;
double TOLERANCE = 1e-4;
double PER_DOC_TOLERANCE= 1e-2;

vector<string> class_list;
vector<string> vocab_list;
vector<vector<short int>> ar_orig_docs;
map<int, vector<short int>> labels_by_ann;
int *class_size;
double *true_rho;
int rho_computed = 0;

double log_likelihood_doc_annotator_observed_class_compute_var_params(Doc* doc, gen_model_params* m_gen,
		int m_topics,
		int m_classes,
		int num_words,
		double *&c_delta,//[TOTAL_CLASSES];
		double ***&m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **&phi,//[num_words][TOTAL_TOPICS];
		double **&delta//[num_words][TOTAL_CLASSES];
){
	double sum_gamma[TOTAL_CLASSES][2], digamma_f_gamma[TOTAL_CLASSES][2][TOTAL_TOPICS];
	double sum_alpha[TOTAL_CLASSES][2], digamma_f_alpha[TOTAL_CLASSES][2][TOTAL_TOPICS];
	//double sum_chi[TOTAL_TOPICS], digamma_f_chi[TOTAL_TOPICS][VOCAB];
	//double sum_eta[TOTAL_TOPICS], digamma_f_eta[TOTAL_TOPICS][VOCAB];

	// populate psi functions for gamma and alpha
	for (int i=0; i< TOTAL_CLASSES; i++){
		for (int j=0; j<=1; j++){
			sum_gamma[i][j] = 0; sum_alpha[i][j]=0;
			for (int t=0; t< TOTAL_TOPICS; t++){
				sum_gamma[i][j] += m_gamma[i][j][t];
				digamma_f_gamma[i][j][t] = gsl_sf_psi(m_gamma[i][j][t]);
				sum_alpha[i][j] += m_gen->alpha[i][j][t];
				digamma_f_alpha[i][j][t] = gsl_sf_psi(m_gen->alpha[i][j][t]);

			}

			for (int t=0; t<TOTAL_TOPICS; t++){
				digamma_f_gamma[i][j][t] -= gsl_sf_psi(sum_gamma[i][j]);
				digamma_f_alpha[i][j][t] -= gsl_sf_psi(sum_alpha[i][j]);
			}
		}
	}
	//cout << " Computed psi for gamma and alpha"<< endl;
	// populate psi functions for chi and eta

	//cout << " Computed psi for chi  and eta"<< endl;
	double elbo = 0;
	for (int i=0; i< TOTAL_CLASSES; i++){
		elbo += (c_delta[i]*(log(max(m_gen->xi[i],1e-6 )) -
				log(max(c_delta[i],1e-6))))+
						((1-c_delta[i])*
								(log(max(1-m_gen->xi[i],1e-6))-log(max(1-c_delta[i],1e-6)) ));
		for (int j=0; j<=1; j++){
			elbo+= gsl_sf_lngamma(sum_alpha[i][j]) - gsl_sf_lngamma(sum_gamma[i][j]);
			for (int t=0; t< TOTAL_TOPICS; t++){
				elbo = elbo + gsl_sf_lngamma(m_gamma[i][j][t])
																- gsl_sf_lngamma(m_gen->alpha[i][j][t]) +
																((m_gen->alpha[i][j][t] - m_gamma[i][j][t])*digamma_f_gamma[i][j][t]);

				for(int n=0; n< doc->num_words; n++){
					elbo+= pow(c_delta[i],j)*
							pow(1-c_delta[i], 1-j)*
							delta[n][i] * phi[n][t]* digamma_f_gamma[i][j][t];
				}
			}
		}
		//	cout << i<<". Finished i-j-t-n. Now out of j loop" << endl;
		for (int n=0; n<doc->num_words; n++){
			elbo += delta[n][i]*(log(m_gen->kappa[i])
					- log(max(delta[n][i],1e-6)));
		}


	}
	//cout << " Finished i-j-t-n. Now out of i loop" << endl;
	for (int t=0; t<TOTAL_TOPICS; t++){
		//cout << "sum_eta:" << sum_eta[t] <<" , sum_chi: "<< sum_chi[t]<<endl;
		//	elbo+= gsl_sf_lngamma(sum_eta[t]) - gsl_sf_lngamma(sum_chi[t]);
		//cout << " Entering j loop" << endl;
		for (int n=0; n<doc->num_words; n++){
			int j=doc->docwords[n];
			elbo+= (phi[n][t]* (log(max(m_gen->beta[t][j],1e-6))
					- log(max(phi[n][t],1e-6))));
		}
	}

	// account for annotators
	if (doc->ann_present_flag == 1){
		for (int j=0; j<TOTAL_ANN; j++){
			for (int i=0; i<TOTAL_CLASSES; i++){

				if (doc->ann_classes[j][i]!=-1){
					elbo += ((c_delta[i]*doc->ann_classes[j][i] )+
							(1-c_delta[i])*(1-doc->ann_classes[j][i]))*
									log(max(m_gen->rho[j],1e-6));
					elbo +=((c_delta[i]*(1-doc->ann_classes[j][i] ))+
							(1-c_delta[i])*(doc->ann_classes[j][i]))*
									log(max(1-m_gen->rho[j],1e-6));
				}
			}
		}
	}
	return elbo;
}

double norm_diff(double* a, double *b, int len){
	double sum= 0;
	for (int i=0; i<len; i++){
		sum+=pow(fabs(a[i] - b[i]),2);
	}
	sum= sqrt(sum*1.0/len);
	return sum;
}


void estep_annotator_model_training(corpus* m_corpus,
		gen_model_params* m_gen, int outer = 0){
	timestamp_t t0 = get_timestamp(); double log_l= 0;
	//    double log_l_corpus =log_likelihood_complete_observed_class(m_corpus,
	//            m_gen, m_complete_var, 1);
	double log_l_corpus_old =m_corpus->log_l;
	//    cout << " E"<<" starting with logl "<< log_l_corpus<<endl;
	int total_num_docs = m_corpus->all_docs.size();
	initialize_suff(TOTAL_CLASSES, TOTAL_TOPICS, VOCAB, TOTAL_ANN);
#pragma omp parallel for
	for (int doc_it = 0; doc_it <total_num_docs; doc_it++)
	{
		//short int doc_id = it->first;

		Doc* doc = m_corpus->all_docs_vec[doc_it];
		//temp variables
		double *c_delta;//[TOTAL_CLASSES];
		double ***m_gamma;//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **phi;//[num_words][TOTAL_TOPICS];
		double **delta;
		double log_l_doc;


		//double log_l_old = log_likelihood_doc_annotator_observed_class_new(doc, m_gen);
		double log_l_old = doc->log_l;

		reset_params(TOTAL_TOPICS, TOTAL_CLASSES, doc->num_words,
				c_delta,//[TOTAL_CLASSES];
				m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
				phi,//[num_words][TOTAL_TOPICS];
				delta//[num_words][TOTAL_CLASSES];
		);
		//doc->m_var_model_doc->reset_params(TOTAL_TOPICS);
		// store the digamma function values of gamma
		// removing m_var_model_doc
		double digamma_temp[TOTAL_CLASSES][2][TOTAL_TOPICS];
		for(int innerEstep = 0; innerEstep < MAX_E_ITER_TRNG ; ++innerEstep)  //for E Step
		{
			// Initialize all variational params. --done

			int doc_count = 0;
			for (int i=0; i< TOTAL_CLASSES; i++){
				for (int j=0; j<=1; j++){
					double digamma_sum= 0;
					for (int t=0; t<TOTAL_TOPICS; t++)
					{
						digamma_temp[i][j][t] = 0;
						digamma_sum += m_gamma[i][j][t];
					}
					digamma_sum = gsl_sf_psi(digamma_sum);
					for (int t=0; t<TOTAL_TOPICS; t++){
						digamma_temp[i][j][t] = gsl_sf_psi(m_gamma[i][j][t]) - digamma_sum;
					}
				}
			}
			for (int i= 0; i< TOTAL_CLASSES; i++){
				double a = log(max(m_gen->xi[i],1e-6)), b=  log(max(1- m_gen->xi[i],1e-6));
				//cout <<" i = "<< i<<" a = "<< a<< " b = "<< b<< endl;
				//cout<<" -------------------------------------------- "<<endl;
				for (int t=0; t<TOTAL_TOPICS; t++){
					for (int n= 0; n< doc->num_words; n++){
						a = a + (delta[n][i]* phi[n][t]*
								digamma_temp[i][1][t]);
						b= b+ (delta[n][i]* phi[n][t]*
								digamma_temp[i][0][t]);
						//cout <<" n = "<< n<< " a = "<< a <<" b = "<<b<<endl;
					}
				}

				// account for the annotators
				for (int j=0; j<TOTAL_ANN; j++){
					if (doc->ann_classes[j][i]!=-1){
						a = a+ (doc->ann_classes[j][i] * log(max(m_gen->rho[j],1e-6)))
                                                                    										+((1-doc->ann_classes[j][i]) * log(max(1-m_gen->rho[j],1e-6)));
						b = b+ (doc->ann_classes[j][i] * log(max(1-m_gen->rho[j],1e-6)))
                                                                    										+((1-doc->ann_classes[j][i]) * log(max(m_gen->rho[j],1e-6)));

					}
				}
				c_delta[i] = 1.0/(1+ exp(b-a));
				//                    cout<<" a= "<< a<< " b= "<< b<<" "<<
				//                     doc->m_var_model_doc->c_delta[i]<<" " <<doc->doc_classes[i]<< endl;


			}

			//   phi_n_t updates
			for (int n=0; n< doc->num_words; n++){

				double temp [ TOTAL_TOPICS] , total[TOTAL_TOPICS];

				//double digamma_sum_1[TOTAL_CLASSES] , digamma_sum_0[TOTAL_CLASSES];
				//for (int i=0; i< TOTAL_CLASSES; i++){
				//    digamma_sum_1[i] = 0;
				for (int t=0; t< TOTAL_TOPICS; t++){
					temp[t] = 0;
					//    digamma_sum_1[i]+= doc->m_var_model_doc->m_gamma[i][1][t];
					//    digamma_sum_0[i]+= doc->m_var_model_doc->m_gamma[i][0][t];
				}
				//}
				for (int t=0; t< TOTAL_TOPICS; t++){

					for (int i=0; i< TOTAL_CLASSES; i++){

						temp[t] += delta[n][i] *((c_delta[i] * (digamma_temp[i][1][t]))//gsl_sf_psi(doc->m_var_model_doc->m_gamma[i][1][t] - gsl_sf_psi(digamma_sum_1[i]))))
								+ ((1-c_delta[i])    * digamma_temp[i][0][t]));
					}
					int j= doc->docwords[n];
					temp[t] += log(max(m_gen->beta[t][j],1e-6));
					//(gsl_sf_psi(m_complete_var.chi[t][doc->docwords[n]]) - gsl_sf_psi(chi_sum));


				}
				int sum_phi = 0;
				for (int t=0; t< TOTAL_TOPICS; t++){
					{
						total[t] =0;
						for (int t1 = 0; t1< TOTAL_TOPICS; t1++)
							total[t] += exp(temp[t1]- temp[t]);
					}
					phi[n][t] = 1.0/(total[t]);
					sum_phi += phi[n][t];
				}
				//static_assert(sum_phi==1, " Sum_phi = " << sum_phi);
			}

			//   delta_n_i updates
			for (int n=0; n< doc->num_words; n++){
				double temp[TOTAL_CLASSES],  total;
				for(int c=0; c< TOTAL_CLASSES; c++)
					temp[c] = log(m_gen->kappa[c]);

				for(int i=0; i< TOTAL_CLASSES; i++){
					for(int t=0; t<TOTAL_TOPICS; t++){


						temp[i] = temp[i]+ ( phi[n][t]*
								((doc->doc_classes[i]*digamma_temp[i][1][t])
										+ (( 1-doc->doc_classes[i] )* digamma_temp[i][0][t])));



					}
				}

				for (int i=0; i< TOTAL_CLASSES; i++){
					{
						total =0;
						for (int i1 = 0; i1< TOTAL_CLASSES; i1++)
							total = total+ exp(temp[i1]- temp[i]);
					}
					delta[n][i] = 1.0/total;
					delta[n][i] = 1.0/TOTAL_CLASSES;
				}

			}


			//   gamma_i_j_t
			for(int i=0; i< TOTAL_CLASSES; i++){
				for (int t=0; t<TOTAL_TOPICS; t++)
				{
					m_gamma[i][0][t] = m_gen->alpha[i][0][t];
					m_gamma[i][1][t] = m_gen->alpha[i][1][t];
					for (int n=0; n< doc->num_words; n++){


						m_gamma[i][1][t]+=(c_delta[i])*
								delta[n][i]*
								phi[n][t];

						m_gamma[i][0][t]+=(1-c_delta[i])*
								delta[n][i]*
								phi[n][t];


					}
				}

			}



			//break;



			/*    if (outer%5==4){
//collapse(2)
            //separate loop for chi updates, chi_t_j updates
            int total_num_docs = m_corpus->all_docs_vec.size();

//#pragma omp parallel for //collapse(2)
            for (int t=0; t< TOTAL_TOPICS; t++){
                for(int j=0; j< VOCAB; j++){
                    m_complete_var->chi[t][j] = m_gen->eta[t][j];
                }
                    for(int doc_it=0; doc_it< total_num_docs; doc_it++){
                        Doc* doc= m_corpus->all_docs_vec[doc_it];
                        for (int n=0; n< doc->num_words; n++){
                            int j = doc->docwords[n];
                            //if (j== doc->docwords[n]){
                            m_complete_var->chi[t][j] += doc->m_var_model_doc->phi[n][t];
                            //}

                        }
                    }

                //}

            }*/
			//cout << " Updated chi "<<endl;
			/*    for(int t=0; t<TOTAL_TOPICS; t++){
                        cout<<" New topic "<< t<< ":"<<endl;
                    for(int j=0; j< VOCAB; j++){
                                        cout<<m_complete_var->chi[t][j]<<endl;
                                    }

                    }*/
			//}

			//break;
			//            log_l =log_likelihood_complete_observed_class(m_corpus,
			//                    m_gen, m_complete_var, 1);
			log_l_doc =
					log_likelihood_doc_annotator_observed_class_compute_var_params(
							doc,
							m_gen,
							TOTAL_TOPICS,
							TOTAL_CLASSES,
							doc->num_words,
							c_delta,//[TOTAL_CLASSES];
							m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
							phi,//[num_words][TOTAL_TOPICS];
							delta//[num_words][TOTAL_CLASSES];
					);

			//           double rel_diff = fabs((log_l - log_l_old)/log_l_corpus);
			//           rel_diff = rel_diff * TOTAL_DOCS;
			double rel_diff = fabs((log_l_doc - log_l_old)/log_l_old);
			doc->log_l= log_l_doc;
			//cout<<" Inner E-logl =" <<log_l<<" Rel_diff = "<<rel_diff<<endl;

			if(outer <=10 && innerEstep >=10)
				break;
			else if (rel_diff< PER_DOC_TOLERANCE )
				break;
		}
		//cout<< doc_it<<" done!" <<endl;
#pragma omp critical
		{
			log_l += (log_l_doc* doc->weightage);
			for (int i=0; i<TOTAL_CLASSES; i++)
			{
				suff1[i] = suff1[i] + (c_delta[i]*doc->weightage/VIRTUAL_TOTAL_DOCS);
				for (int j=0; j<TOTAL_ANN; j++){
					if (doc->ann_present_flag==1 && doc->ann_classes[j][i]!=-1){
						suff2[j] +=
								(doc->ann_classes[j][i]*c_delta[i])
								+ ((1-doc->ann_classes[j][i])*(1-c_delta[i]));
						suff3[j]++;
					}
				}
			}
			for (int n=0; n<doc->num_words; n++){
				int j=doc->docwords[n];

				for (int t=0; t<TOTAL_TOPICS; t++)
					suff4[t][j] += phi[n][t];
			}

			double digamma_temp[TOTAL_CLASSES][2];
			for (int i=0; i<TOTAL_CLASSES; i++){
				digamma_temp[i][0] = 0;
				digamma_temp[i][1] = 0;
				for(int t=0; t<TOTAL_TOPICS; t++){
					digamma_temp[i][0] += m_gamma[i][0][t];
					digamma_temp[i][1] += m_gamma[i][1][t];

				}

				for (int t=0; t<TOTAL_TOPICS; t++){
					suff5[i][0][t] += gsl_sf_psi( m_gamma[i][0][t])
        															- gsl_sf_psi(digamma_temp[i][0]);
					suff5[i][1][t] += gsl_sf_psi( m_gamma[i][1][t])
            															- gsl_sf_psi(digamma_temp[i][1]);
				}
			}

		}
		delete_params(TOTAL_TOPICS, TOTAL_CLASSES, doc->num_words,
				c_delta,//[TOTAL_CLASSES];
				m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
				phi,//[num_words][TOTAL_TOPICS];
				delta//[num_words][TOTAL_CLASSES];
		);

	} // for every doc
	// compute log likelihood;
	log_l = log_l*1.0/m_corpus->compute_virtual_num_docs();
	cout<<" E-log_l_old"<< log_l_corpus_old <<" logl_new =" <<log_l << endl;
	m_corpus->log_l = log_l;
	timestamp_t t1 = get_timestamp();
	double secinner = (t1 - t0) / 1000000.0L;
	cout<< " Time for E = "<< secinner<< endl;

}

void mstep_annotator_model_training(corpus* m_corpus, gen_model_params* m_gen){
	long seed = time(NULL);
	gsl_rng *r_generator = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r_generator, seed);
	timestamp_t t1 = get_timestamp();

	// Initialize xi, alpha, eta, rho (For annotator model)
	/*for (int i=0; i<TOTAL_CLASSES; i++){
		for (int j=0; j<=1; j++){
			for (int t=0; t<TOTAL_TOPICS; t++)
				m_gen->alpha[i][j][t] = gsl_ran_gamma(r_generator, 1,1);
		}
	}
	for (int t=0; t<TOTAL_TOPICS; t++){
		for (int j=0; j<VOCAB; j++){
			m_gen->eta[t][j] = gsl_ran_beta(r_generator, 1,1);
		}
	}*/
	//M Step starts
	// xi_i updates
	// rho_i updates
	// Newton Rhapson updates for alpha_i_j_r
	// Newton Rhapson updates for eta_i_j
	//M Step starts
	// xi_i updates
#pragma omp parallel for
	for(int i=0; i< TOTAL_CLASSES; i++)
	{
		//		m_gen->xi[i] = 0;
		//		for(map<short int, Doc*> ::iterator it = m_corpus->all_docs.begin(); it != m_corpus->all_docs.end(); it++)
		//		{
		//			//short int doc_id = it->first;
		//			Doc* doc = it->second;
		//
		//			m_gen->xi[i] = m_gen->xi[i] +  ((doc->weightage*doc->m_var_model_doc->c_delta[i]*1.0)/VIRTUAL_TOTAL_DOCS);//(doc->m_var_model_doc->c_delta[i]*1.0/TOTAL_DOCS);
		//
		//			//cout<< doc->m_var_model_doc->c_delta[i]<<endl;
		//		}
		//		//cout << " i= " <<i <<", xi[i] = "<< m_gen->xi[i]<<" , suff1 "<< suff1[i]<<endl;
		//		assert(fabs(m_gen->xi[i]-suff1[i])<TOLERANCE);
		m_gen->xi[i]=suff1[i];
	}

	// compute log likelihood;

	//#pragma omp parallel for
	for(int j=0; j < TOTAL_ANN; ++j)
	{
		//		m_gen->rho[j]=0;
		//		double total_sum = 0;
		//		for(map<short int, Doc*> ::iterator it = m_corpus->all_docs.begin();
		//				it != m_corpus->all_docs.end(); it++)
		//		{
		//			//short int doc_id = it->first;
		//			Doc* doc = it->second;
		//
		//			//cout << " Ann present flag = "<<doc->ann_present_flag<<endl;
		//			for(int i=0; i<TOTAL_CLASSES; ++i)
		//			{
		//				//cout <<i<<"."<< doc->ann_classes[j][i]<< " "<< " c_Delta = " << doc->m_var_model_doc->c_delta[i]<<endl;
		//				if (doc->ann_classes[j][i]!=-1){
		//
		//					total_sum += doc->weightage;//doc->ann_present_flag;
		//					m_gen->rho[j] += doc->ann_present_flag*doc->weightage*( (doc->ann_classes[j][i]* doc->m_var_model_doc->c_delta[i])+
		//							((1-doc->ann_classes[j][i])*(1 - doc->m_var_model_doc->c_delta[i])));
		//
		//
		//				}
		//			}
		//			//cout << endl;
		//
		//		}
		//		//		cout<< " rho "<<j << " = " << m_gen->rho[j] << " total_sum = " << total_sum
		//		//				<<" no. of labels given = "<< labels_by_ann[j].size()<<endl;
		//		m_gen->rho[j] = m_gen->rho[j]*1.0/total_sum;
		//		assert(fabs(m_gen->rho[j] - (suff2[j]*1.0/suff3[j]))<TOLERANCE);
		m_gen->rho[j] = (suff2[j]*1.0/suff3[j]);
	}
	//exit(0);



	// Newton Rhapson updates for alpha_i_j_r

	int break_from_nr_alpha = 0;
#pragma omp parallel for collapse(2)
	for (int i=0; i<TOTAL_CLASSES; ++i)
	{
		for(int j=0; j<2; j++)
		{
			for(int nr_for_alpha = 0; nr_for_alpha < MAX_NR_ITERATIONS; ++nr_for_alpha)
			{
				double sum_all = 0.0;
				for(int tIdx = 0; tIdx < TOTAL_TOPICS; ++tIdx){
					sum_all += m_gen->alpha[i][j][tIdx];
				}
				// sum_all = sum(m_gen.alpha[i][j][.]);
				double z = VIRTUAL_TOTAL_DOCS * (gsl_sf_psi_n(1,sum_all));
				double g[TOTAL_TOPICS], h[TOTAL_TOPICS], c_denom = 1.0/z, c_num=0.0, c, sum_gr=0;
				//#pragma omp parallel for reduction (+: c_num,c_denom, sum_gr)
				for(int r=0; r<TOTAL_TOPICS; ++r)
				{

					//					g[r] = 0;
					//					for(map<short int, Doc*> ::iterator it = m_corpus->all_docs.begin(); it != m_corpus->all_docs.end(); it++)
					//					{
					//						//short int doc_id = it->first;
					//						Doc* doc = it->second;
					//						double sum_doc_all = 0.0;
					//						for(int tIdx = 0; tIdx < TOTAL_TOPICS; ++tIdx){
					//							sum_doc_all += doc->m_var_model_doc->m_gamma[i][j][tIdx];
					//						}
					//						//sum_doc_all = sum(doc->m_var_model_doc->m_gamma[i][j][.]);
					//						g[r] += doc->weightage*(gsl_sf_psi_n(0,doc->m_var_model_doc->m_gamma[i][j][r])- gsl_sf_psi_n(0, sum_doc_all));
					//					}
					//					assert(fabs(g[r] -suff5[i][j][r])<TOLERANCE);
					g[r] = suff5[i][j][r];
					g[r] += VIRTUAL_TOTAL_DOCS *(gsl_sf_psi_n(0,sum_all)- gsl_sf_psi_n(0, m_gen->alpha[i][j][r]));

					h[r] = -VIRTUAL_TOTAL_DOCS * gsl_sf_psi_n(1, m_gen->alpha[i][j][r]);
					c_denom = c_denom + 1/h[r];
					c_num += (g[r]/h[r]);
					sum_gr += pow(g[r],2);
				}
				sum_gr = sqrt(sum_gr);
				c = c_num/c_denom;
				if (sum_gr< TOLERANCE)
					break;
				double old_r_val[TOTAL_TOPICS];
				for(int r=0; r<TOTAL_TOPICS; ++r)
					old_r_val[r] = m_gen->alpha[i][j][r];
				int reset_old= 0;
				//#pragma omp parallel for
				for(int r=0; r<TOTAL_TOPICS; ++r)
				{

					m_gen->alpha[i][j][r] -= (g[r]-c)/h[r];
					if (m_gen->alpha[i][j][r]<=0 )
					{
						reset_old = 1;
						m_gen->alpha[i][j][r] = 1e-6;// old_val;
						//	break_from_nr_alpha = 1;
					}
				}
				/*if (reset_old == 1){
					//cout << " Resetting  alpha "<< endl;
					for(int r=0; r<TOTAL_TOPICS; ++r)
						m_gen->alpha[i][j][r]= old_r_val[r];
					break;
				}
				 */
				//cout << " sum_ gr = "<<sum_gr<<endl;

			}
		}
		//break;
		//if (break_from_nr_alpha == 1)
		//	break;
	}

	//	cout << " NR for alpha done! "<< endl;


	// Updates for beta
	int total_num_docs = m_corpus->all_docs_vec.size();
	//	for (int t=0; t<TOTAL_TOPICS; t++){
	//		for (int j=0; j<VOCAB; j++){
	//			m_gen->beta[t][j] =1.0/(VOCAB*VOCAB*VOCAB);
	//		}
	//		double temp = 0;
	//		for (int doc_it =0 ; doc_it<total_num_docs; doc_it++){
	//			int deno =0;
	//			Doc* doc = m_corpus->all_docs_vec[doc_it];
	//			for (int n=0; n<doc->num_words; n++){
	//				int j = doc->docwords[n];
	//				m_gen->beta[t][j] = m_gen->beta[t][j] +
	//						//(doc->m_var_model_doc->phi[n][t]/(total_num_docs+VOCAB));
	//						((doc->weightage*doc->m_var_model_doc->phi[n][t]));///(total_num_docs*doc->num_words));///(total_num_docs+VOCAB));
	//			}
	//
	//
	//		}
	//		for(int j=0; j<VOCAB; j++)
	//			temp += m_gen->beta[t][j];
	//		for(int j=0; j<VOCAB; j++){
	//			assert(fabs(m_gen->beta[t][j] - (1.0/(VOCAB*VOCAB*VOCAB)) - suff4[t][j])<TOLERANCE);
	//			m_gen->beta[t][j] = m_gen->beta[t][j]/temp;
	//		}
	for (int t=0; t<TOTAL_TOPICS; t++){
		double temp = 0;
		for (int j=0; j<VOCAB; j++){
			m_gen->beta[t][j] = (1.0/(VOCAB*VOCAB*VOCAB)) + suff4[t][j];
			temp += m_gen->beta[t][j];
		}
		for(int j=0; j<VOCAB; j++)
			m_gen->beta[t][j] = m_gen->beta[t][j]/temp;

	}

	// update for kappa_i
	total_num_docs = m_corpus->all_docs_vec.size();
	//	for (int i=0; i<TOTAL_CLASSES; i++){
	//		m_gen->kappa[i] = 0;
	//	}

	for (int i=0; i<TOTAL_CLASSES; i++){
		//		double temp = 0;
		//#pragma omp parallel for reduction (+: temp)
		//		for (int doc_it =0 ;doc_it<total_num_docs; doc_it++){
		//			Doc* doc = m_corpus->all_docs_vec[doc_it];
		//			for (int n=0; n<doc->num_words; n++){
		//				temp += ((doc->m_var_model_doc->delta[n][i])*1.0/(doc->num_words*TOTAL_DOCS));
		//			}
		//		}
		//		m_gen->kappa[i] = temp;
		m_gen->kappa[i] = 1.0/TOTAL_CLASSES;
	}
	//	cout << " NR for eta done! "<< endl;
	timestamp_t t2 = get_timestamp();
	double secinner = (t2 - t1) / 1000000.0L;
	cout<< " Time for M = "<< secinner<<endl;
	gsl_rng_free(r_generator);
}

vector<string >em_annotator_model_training(corpus* m_corpus, gen_model_params* m_gen, 
		string src_folder, string output_file ){
	// lambda is observed here.
	double log_l=0;
	string model_filename;
	/*log_l =log_likelihood_complete_observed_class(m_corpus,
			m_gen, m_complete_var, 1);
	cout << " Initial log-l = "<< log_l<<endl;*/
	string logl_h_name = src_folder;
	boost::uuids::uuid uuid = boost::uuids::random_generator()();
	string folder_name = "model_ann/";
	folder_name.append(to_string(uuid));
	folder_name.append("/");
	string model_folder = src_folder; model_folder.append("model_ann/");
	mkdir(model_folder.c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH | S_IWOTH );
	string logl_h_folder = src_folder; logl_h_folder.append(folder_name);
	mkdir(logl_h_folder.c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH | S_IWOTH );
	fstream logl_h;
	string file_h = logl_h_folder; file_h.append("loglikelihood_non_ann_training.dat");
	logl_h.open(file_h.c_str(), fstream::out);
	cout<<" Created "<<logl_h_folder<< endl;
	double log_l_init = -1e-6;
	create_and_initialize_suff(TOTAL_CLASSES, TOTAL_TOPICS, VOCAB, TOTAL_ANN);
	for(int outer = 0; outer < MAX_EM_ITER ; ++outer)   //for entire EM
	{

		/*if ((outer%4 == 1) )
			continue;*/

		estep_annotator_model_training(m_corpus,  m_gen, outer);

		cout << " E"<< outer<< " done. " << endl;

		log_l =m_corpus->log_l; // TODO
		logl_h<< " E"<< outer<< ": "<< log_l<<endl;
		cout << " E"<< outer<< ": "<< log_l<<endl;

		mstep_annotator_model_training(m_corpus, m_gen); // TODO
		cout << " M"<< outer<< " done. " << endl;
		// compute log likelihood;

		//		log_l =log_likelihood_complete_observed_class(m_corpus,
		//				m_gen, m_complete_var, 1); //TODO
		//		logl_h<< " M"<< outer<< ": "<< log_l<<endl;
		//		cout << " M"<< outer<< ": "<< log_l<<endl;

		if (outer%4 == 3){
			if ((abs((log_l -log_l_init)/log_l_init))<TOLERANCE)
			{
				string param_file_name = "param_";
				param_file_name.append(to_string(outer));
				string param_folder = logl_h_folder; param_file_name.append(".dump");
				m_gen->save_params_to_file(param_file_name.c_str(), param_folder);
				model_filename = param_folder+param_file_name;
				m_gen->save_params_to_file(output_file, "./");				
				break;
			}
			else{
				cout<< " Relative diff = " << (abs((log_l -log_l_init)/log_l_init))<< endl;
				log_l_init = log_l;
			}
		}
		if (outer%2 == 0){
			string param_file_name = "param_";
			param_file_name.append(to_string(outer));
			string param_folder = logl_h_folder; param_file_name.append(".dump");
			m_gen->save_params_to_file(param_file_name.c_str(), param_folder);
			model_filename = param_folder+param_file_name;
		}

	}
	remove(output_file.c_str());
	m_gen->save_params_to_file(output_file, "./");
	logl_h.close();
	vector<string> model_folder_filename;
	model_folder_filename.push_back(logl_h_folder);
	model_folder_filename.push_back(model_filename);
	return model_folder_filename;
}


void simulate_annotators_for_doc(Doc* doc, int m_ann,gsl_rng *r,
		string output_dir = "/home/divya/workspace/multiLabelCrowd/src/phase7/", double* rho=NULL){
	TOTAL_ANN= m_ann;
	//	long seed = time(NULL);
	//	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);
	//	gsl_rng_set(r, seed);
	short int doc_id = doc->doc_id;
	mkdir(output_dir.c_str(),  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH | S_IWOTH );

	fstream f_output; string file_name = output_dir;
	file_name.append(to_string(doc_id));
	file_name.append("_ann.txt");
	f_output.open(file_name.c_str(), fstream::out);

	f_output<<"TOTAL_ANN: \t"<< TOTAL_ANN<<endl;
	f_output<<"rho: "<<endl;
	if (rho==NULL)
	{
		rho= new double[TOTAL_ANN];
		//seed = time(NULL);
		//gsl_rng_set(r, seed);
		for (int j=0; j<TOTAL_ANN; j++){
			rho[j] = gsl_ran_beta(r, 1.0, 1.0);
			if (rho[j]<0.5)
				rho[j] = 1 - rho[j];

		}
	}
	for (int j=0; j<TOTAL_ANN; j++){
		f_output<< rho[j]<<endl;
	}


	doc->m_total_ann = m_ann;
	if (doc->ann_classes==NULL)
		doc->ann_classes= new int*[TOTAL_ANN];
	doc->ann_present_flag = 1;
	f_output<<"Doc_id: \t"<< doc_id<<endl;
	f_output<<"Classes:"<<endl;

	int ann_list[TOTAL_ANN];
	for (int i=0; i < TOTAL_ANN; i++){
		ann_list[i] = i;
	}
	gsl_ran_shuffle (r, ann_list, TOTAL_ANN, sizeof(int));

	int num_ann_chosen =0.5*TOTAL_ANN;
	for (int j=0; j<TOTAL_ANN; j++){
		if (doc->ann_classes[j]==NULL)
		{
			doc->ann_classes[j] = new int[TOTAL_CLASSES];
			cout <<" Should not be here!"<<endl;
			exit(0);
		}
	}
	for (int j=0 ; j<num_ann_chosen; j++){
		int ann  = ann_list[j];
		labels_by_ann[ann].push_back(doc->doc_id);
		//cout << " Ann = "<< ann << " , rho = "<< rho[ann]<<endl;
		for (int i=0; i< TOTAL_CLASSES; i++){
			int true_or_not = gsl_ran_bernoulli(r, rho[ann]);
			doc->ann_classes[ann][i] = (true_or_not*doc->doc_classes[i]) +
					((1-true_or_not)*(1-doc->doc_classes[i]));
			//	cout<< doc->doc_classes[i]<<"<->"<<doc->ann_classes[ann][i] << " ";
			//f_output<<doc->ann_classes[j][i] << " ";
		}
		//cout << rho[j]<< endl;
		//f_output<<endl;
	}
	for (int j=num_ann_chosen; j<TOTAL_ANN; j++){
		int ann  = ann_list[j];
		for (int i=0; i< TOTAL_CLASSES; i++){
			doc->ann_classes[ann][i] = -1;
		}
	}

	for (int j=0; j< TOTAL_ANN; j++){
		for (int i=0; i<TOTAL_CLASSES; i++){
			f_output<<doc->ann_classes[j][i] << " ";
		}
		f_output<<endl;
	}
	f_output.close();
	//	gsl_rng_free(r);
}

void simulate_annotators_for_corpus(corpus* corpus_arg, string src_folder, int m_ann){
	string phase7_dir = src_folder; phase7_dir.append("ann_labels/");
	string rho_file = src_folder + "true_rho.txt";
	fstream f_rho; f_rho.open(rho_file.c_str(), ios::out);
	f_rho<<"TOTAL_ANN: \t "<< m_ann<<endl;
	cout<<phase7_dir<<endl;
	long seed = time(NULL);
	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r, seed);
	double rho[m_ann]; TOTAL_ANN = m_ann;
	int frac_m_1 = 0.2*TOTAL_ANN;
	int frac1 = 0.3*TOTAL_ANN;
	int frac2 = 0.4*TOTAL_ANN;
	if (true_rho==NULL)
		true_rho = new double[TOTAL_ANN];
	for (int j=0; j<frac_m_1; j++){

		rho[j] = gsl_ran_flat (r, 0.0001, 0.1);
		true_rho[j] = rho[j];
	}
	for (int j=frac_m_1; j<frac1; j++){

		rho[j] = gsl_ran_flat (r, 0.51, 0.65);
		if (rho[j]<0.5)
			rho[j] = 1 - rho[j];
		true_rho[j] = rho[j];

	}
	for (int j=frac1; j<frac1+frac2; j++){

		rho[j] = gsl_ran_flat (r, 0.66, 0.85);
		if (rho[j]<0.5)
			rho[j] = 1 - rho[j];
		true_rho[j] = rho[j];
	}
	for (int j=frac1+frac2; j<TOTAL_ANN; j++){

		rho[j] = gsl_ran_flat (r, 0.86, 0.9999);
		if (rho[j]<0.5)
			rho[j] = 1 - rho[j];
		true_rho[j] = rho[j];
	}
	rho_computed=1;
	// write rho to file
	f_rho<<"rho:" <<endl;
	for (int j=0; j<TOTAL_ANN; j++){
		f_rho<<true_rho[j]<<endl;
	}
	int doc_count = 0;
	for (map<short int, Doc*> ::iterator it = corpus_arg->all_docs.begin();
			it != corpus_arg->all_docs.end();
			it++, doc_count++){

		Doc* curr_doc = it->second;
		simulate_annotators_for_doc(curr_doc, TOTAL_ANN,r, phase7_dir, rho);

	}
	gsl_rng_free(r);
	f_rho.close();
}
void load_ann_labels_for_doc(Doc* curr_doc, string filename, int m_ann, int m_classes){
	fstream f_input;
	//Allocate doc.ann_classes if not already done.

	if (curr_doc->ann_classes==NULL)
	{
		curr_doc->ann_classes=new int*[m_ann];
		for(int i=0; i<m_ann; i++)
			curr_doc->ann_classes[i] = new int[m_classes];

	}
	if (true_rho==NULL)
		true_rho=new double[m_ann];
	curr_doc->ann_present_flag=1;
	f_input.open(filename.c_str(), fstream::in);
	int total_ann_flag=0, rho_flag=0, doc_id_flag=0, classes_flag=0, rho_counter=0;
	int curr_ann=0, curr_class=0;
	while(!f_input.eof()){
		string line;f_input >> line;


		if(line.compare("TOTAL_ANN:")==0){
			total_ann_flag=1; rho_flag=0; doc_id_flag=0; classes_flag=0; continue;
		}
		if(line.compare("rho:")==0){
			total_ann_flag=0; rho_flag=1; doc_id_flag=0; classes_flag=0;
			rho_counter=0;continue;
		}
		if(line.compare("Doc_id:")==0){
			total_ann_flag=0; rho_flag=0; doc_id_flag=1; classes_flag=0;continue;
		}
		if(line.compare("Classes:")==0){
			total_ann_flag=0; rho_flag=0; doc_id_flag=0; classes_flag=1;
			curr_ann=0; curr_class=0;continue;
		}
		if (total_ann_flag==1 || doc_id_flag==1)
		{
			total_ann_flag=0; rho_flag=0; doc_id_flag=0; classes_flag=0;
			continue;
		}
		if (rho_flag==1){
			if (rho_computed==0)
				true_rho[rho_counter] = stold(line);
			rho_counter++;
			if (rho_counter== m_ann){
				total_ann_flag=0; rho_flag=0; doc_id_flag=0; classes_flag=0;
				continue;
			}
		}
		if (classes_flag==1){
			curr_doc->ann_classes[curr_ann][curr_class] = stoi(line);
			curr_class++;
			if(curr_class==m_classes){
				curr_class=0; curr_ann++;
				if(curr_ann==m_ann){
					classes_flag=0; break;
				}
			}
		}
	}
	rho_computed=1;
	f_input.close();
}
void load_ann_labels_from_folder(corpus* corpus_arg, int m_ann, int m_classes, string ann_labels_folder){
	for (map<short int, Doc*> ::iterator it = corpus_arg->all_docs.begin();
			it != corpus_arg->all_docs.end();
			it++){

		Doc* curr_doc = it->second;
		short int doc_id = it->first;
		string file_name = ann_labels_folder + to_string(doc_id)+"_ann.txt";
		//	simulate_annotators_for_doc(curr_doc, TOTAL_ANN,r, phase7_dir, rho);
		load_ann_labels_for_doc(curr_doc, file_name, m_ann, m_classes);

	}
}

//same as estep_non_annotator_model_test
void estep_annotator_model_test(Doc* doc, gen_model_params* m_gen, string model_folder  ){
	double sum_gamma[TOTAL_CLASSES][2], digamma_f_gamma[TOTAL_CLASSES][2][TOTAL_TOPICS];
	double sum_alpha[TOTAL_CLASSES][2], digamma_f_alpha[TOTAL_CLASSES][2][TOTAL_TOPICS];

	double *c_delta;//[TOTAL_CLASSES];
	double ***m_gamma;//[TOTAL_CLASSES][2][TOTAL_TOPICS];
	double **phi;//[num_words][TOTAL_TOPICS];
	double **delta;
	double log_l_doc;

	reset_params(TOTAL_TOPICS, TOTAL_CLASSES, doc->num_words,
			c_delta,//[TOTAL_CLASSES];
			m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
			phi,//[num_words][TOTAL_TOPICS];
			delta//[num_words][TOTAL_CLASSES];
	);

	//double log_l_old = log_likelihood_doc_annotator_observed_class_new(doc, m_gen);
	double log_l_old =log_likelihood_doc_test(
			doc,
			m_gen,
			TOTAL_TOPICS,
			TOTAL_CLASSES,
			doc->num_words,
			c_delta,//[TOTAL_CLASSES];
			m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
			phi,//[num_words][TOTAL_TOPICS];
			delta//[num_words][TOTAL_CLASSES];
	);



	// populate psi functions for alpha
	for (int i=0; i< TOTAL_CLASSES; i++){
		for (int j=0; j<=1; j++){
			sum_gamma[i][j] = 0; sum_alpha[i][j]=0;
			for (int t=0; t< TOTAL_TOPICS; t++){
				sum_alpha[i][j] += m_gen->alpha[i][j][t];
				digamma_f_alpha[i][j][t] = gsl_sf_psi(m_gen->alpha[i][j][t]);

			}

			for (int t=0; t<TOTAL_TOPICS; t++){
				digamma_f_alpha[i][j][t] -= gsl_sf_psi(sum_alpha[i][j]);
			}
		}
	}

	int converged =0; int num_iter = 0;
	double log_l=0; double log_l_init=0;

	while (converged ==0 && num_iter < MAX_E_ITER_TEST ){
		// Estep for loop. till convergence.
		//populate digamma function for gamma.
		for (int i=0; i< TOTAL_CLASSES; i++){
			for (int j=0; j<=1; j++){
				sum_gamma[i][j] = 0;
				for (int t=0; t< TOTAL_TOPICS; t++){
					sum_gamma[i][j] += m_gamma[i][j][t];
					digamma_f_gamma[i][j][t] = gsl_sf_psi(m_gamma[i][j][t]);

				}

				for (int t=0; t<TOTAL_TOPICS; t++){
					digamma_f_gamma[i][j][t] -= gsl_sf_psi(sum_gamma[i][j]);
					//	cout <<" Digamma "<<i <<","<<j<<", "<<t<<" = "<<digamma_f_gamma[i][j][t]<<endl;
				}
			}
		}
		for (int i= 0; i< TOTAL_CLASSES; i++){
			double a = log(max(m_gen->xi[i],1e-6)), b=  log(max(1- m_gen->xi[i],1e-6));
			//cout <<" i = "<< i<<" a = "<< a<< " b = "<< b<< endl;
			//cout<<" -------------------------------------------- "<<endl;
			for (int t=0; t<TOTAL_TOPICS; t++){
				for (int n= 0; n< doc->num_words; n++){
					a = a + (delta[n][i]* phi[n][t]*
							digamma_f_gamma[i][1][t]);
					b= b+ (delta[n][i]* phi[n][t]*
							digamma_f_gamma[i][0][t]);
					//cout <<" n = "<< n<< " a = "<< a <<" b = "<<b<<endl;
				}
			}
			c_delta[i] = 1.0/(1+ exp(b-a));
			//cout<<num_iter<<". a= "<< a<< " b= "<< b<<" "<<
			// c_delta[i]<<" " <<doc->doc_classes[i]<< endl;


		}
		// compute delta

		for (int n=0; n<doc->num_words; n++){
			double *temp = new double[TOTAL_CLASSES];
			for (int i=0; i< TOTAL_CLASSES; i++)
				temp[i] =m_gen->kappa[i];//1.0/TOTAL_CLASSES;

			for (int i=0; i<TOTAL_CLASSES; i++){
				for (int j=0; j<=1; j++){
					for (int t=0; t<TOTAL_TOPICS; t++){
						temp[i] += pow(c_delta[i],j) *
								pow(1-c_delta[i], 1-j) *
								phi[n][t] * digamma_f_gamma[i][j][t];
					}
				}

			}

			for (int i=0; i<TOTAL_CLASSES; i++){
				double total_val = 0;
				for (int j=0; j<TOTAL_CLASSES; j++){
					total_val += exp(temp[j]- temp[i]);
				}
				delta[n][i] = 1.0/(total_val);
				delta[n][i] = 1.0/TOTAL_CLASSES;
				//cout<<" delta[ "<<n<<" ]["<<i<<"] = "<<delta[n][i] << endl;
			}

			delete[] temp;
		}

		// compute phi
		for (int n=0; n<doc->num_words; n++){
			double *temp = new double[TOTAL_TOPICS];
			int j =doc->docwords[n];

			for (int t=0; t<TOTAL_TOPICS; t++){
				temp[t] = log(max(m_gen->beta[t][j],1e-6));
				for (int i=0; i<TOTAL_CLASSES; i++){
					temp[t] += c_delta[i] * delta[n][i] *
							digamma_f_gamma[i][1][t];
					temp[t] += (1-c_delta[i]) * delta[n][i] *
							digamma_f_gamma[i][0][t];

				}
			}
			for (int t=0; t<TOTAL_TOPICS; t++){
				double total_val = 0;
				for (int j=0; j<TOTAL_TOPICS; j++){
					total_val += exp(temp[j]- temp[t]);
				}
				phi[n][t] = 1.0/(total_val);
			}
			delete[] temp;
		}

		// compute gamma
		for (int i=0; i< TOTAL_CLASSES; i++){
			for (int j=0; j<=1; j++){
				for (int t=0; t<TOTAL_TOPICS; t++){
					m_gamma[i][j][t] = m_gen->alpha[i][j][t];
					for (int n=0; n<doc->num_words; n++){
						m_gamma[i][j][t] +=
								pow(c_delta[i], j)*
								pow(1-c_delta[i],1- j) *
								delta[n][i] * phi[n][t];
					}
				}
			}
		}

		/*for (int t=0; t<TOTAL_CLASSES; t++){
			for (int j=0 ; j< VOCAB; j++){
				m_complete_var->chi[t][j] = m_gen->eta[t][j];
			}
			for (int n=0; n< doc->num_words; n++){
				int j= doc->docwords[n];
				m_complete_var->chi[t][j] += phi[n][t];


			}

		}*/

		log_l_init= log_l;
		log_l=  log_likelihood_doc_test(
				doc,
				m_gen,
				TOTAL_TOPICS,
				TOTAL_CLASSES,
				doc->num_words,
				c_delta,//[TOTAL_CLASSES];
				m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
				phi,//[num_words][TOTAL_TOPICS];
				delta//[num_words][TOTAL_CLASSES];
		);
		if(num_iter !=0 )
			converged = (fabs((log_l-log_l_init)/log_l_init) < TOLERANCE);
		num_iter++;
	}

	string test_result_filename = model_folder + to_string(doc->doc_id);
	//test_result_filename.append(to_string(doc_id));
	test_result_filename = test_result_filename + ".txt";
	fstream test_result;

	test_result.open(test_result_filename.c_str(), fstream::out);
	test_result<< doc->doc_id<< " "<<endl;


	for (int i = 0; i< TOTAL_CLASSES; i++)
	{
		test_result<< doc->doc_classes[i]<<" "<< c_delta[i]<<endl;
	}


	test_result.close();
	//cout<<test_result<<endl;

	delete_params(TOTAL_TOPICS, TOTAL_CLASSES, doc->num_words,
			c_delta,//[TOTAL_CLASSES];
			m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
			phi,//[num_words][TOTAL_TOPICS];
			delta//[num_words][TOTAL_CLASSES];
	);


	//cout<< " Converged in "<< num_iter<<" iterations"<<endl;
}


void run_annotator_model(string settings_file, string output_file){
	settings inp_settings = read_from_settings_file(settings_file);
	print_settings(inp_settings);
	corpus *m_test_corpus = new corpus(0);
	corpus *m_training_corpus= new corpus(0);


	int idRead = 0;
	double *bins = NULL;
	if(inp_settings.svm_DirectWords==0){
		string filepath=inp_settings.outer_src+ inp_settings.svm_delim_file;
		//cout<<filepath;
		fstream file_h;
		cout<<"filepath" <<filepath<<endl;
		file_h.open(filepath.c_str(), fstream::in);
		if (!file_h.good()){
			cout  << " The file "<< filepath << " was not found! Please "
					"correct the settings file. "<< endl;
			exit(0);
		}
		bins= new double[inp_settings.svm_vocab_size];
		while(!file_h.eof()&&idRead!=inp_settings.svm_vocab_size){
			string line; file_h >> line;
			bins[idRead]= stod(line);
			idRead++;
		}
		file_h.close();
	}
	//for(int i=0; i<inp_settings.vocab_size; ++i)
	//	cout<<bins[i]<<endl;

	cout<<"end of bin reads" <<endl;
	TOTAL_CLASSES = inp_settings.svm_classes;
	if(inp_settings.svm_topics==0)
		TOTAL_TOPICS = TOTAL_CLASSES ;
	else
		TOTAL_TOPICS = inp_settings.svm_topics;
	VOCAB = inp_settings.svm_vocab_size;

	TOTAL_ANN = inp_settings.num_ann;
	string filepath = inp_settings.outer_src+inp_settings.svm_trng_src;
	//read_svm_file_to_get_corpus_scene(m_training_corpus, filepath, inp_settings,bins);
	read_svm_file_to_get_corpus(m_training_corpus, filepath, inp_settings,bins);

	TOTAL_DOCS = m_training_corpus->all_docs.size();
	VIRTUAL_TOTAL_DOCS = TOTAL_DOCS;

	cout<<m_training_corpus->all_docs.size()<<endl;
	filepath = inp_settings.outer_src+inp_settings.svm_test_src;
	//read_svm_file_to_get_corpus_scene(m_test_corpus, filepath, inp_settings,bins);
	read_svm_file_to_get_corpus(m_test_corpus, filepath, inp_settings,bins);

	cout<<m_test_corpus->all_docs.size()<<endl;
	cout<<TOTAL_TOPICS<<endl;

	timestamp_t time_now =  get_timestamp();

	string model_summaries = inp_settings.outer_src + "model_run_summary_ann_" +to_string(time_now)+".txt";
	string model_results_file = inp_settings.outer_src + "model_run_results_ann_"+  to_string(time_now)+".txt";
	fstream file_h_summary; file_h_summary.open(model_summaries.c_str(), fstream::out);
	fstream file_h_results; file_h_results.open(model_results_file.c_str(), fstream::out);

	if (inp_settings.ann_folder.empty())
	{
		cout << " simulating annotators " << endl;
		simulate_annotators_for_corpus(m_training_corpus, inp_settings.outer_src, TOTAL_ANN );
	}
	else
	{
		cout << "loading annotator labels " << endl;

		load_ann_labels_from_folder(m_training_corpus, TOTAL_ANN, TOTAL_CLASSES,inp_settings.ann_folder);
		// write true_rho to file.
		string rho_file = inp_settings.outer_src + "true_rho.txt";
		fstream f_rho; f_rho.open(rho_file.c_str(), ios::out);
		f_rho<<"TOTAL_ANN: \t "<< TOTAL_ANN<<endl;
		f_rho<<"rho:" <<endl;
		for (int j=0; j<TOTAL_ANN; j++){
			f_rho<<true_rho[j]<<endl;
		}
		f_rho.close();


	}

	///////Very Very important, create vector copies ///////

	for (map<short int, Doc*> ::iterator it = m_test_corpus->all_docs.begin(); it != m_test_corpus->all_docs.end();
			it++){
		m_test_corpus->all_docs_vec.push_back(it->second);
	}
	for (map<short int, Doc*> ::iterator it = m_training_corpus->all_docs.begin(); it != m_training_corpus->all_docs.end();
			it++){
		m_training_corpus->all_docs_vec.push_back(it->second);
	}
	cout << " Full set has "<< m_training_corpus->all_docs_vec.size() << " documents!"<<endl;


	//for (int run_count = 0; run_count<num_runs; run_count++){


	m_training_corpus->reset_weights_of_docs();


	gen_model_params m_gen(TOTAL_CLASSES, TOTAL_TOPICS, VOCAB, TOTAL_ANN);



	//				for (map<short int, Doc*> ::iterator it = fraction_training_corpus.all_docs.begin(); it != fraction_training_corpus.all_docs.end();
	//						it++){
	//					Doc* curr_doc=  it->second;
	//					curr_doc->reset_var_params(TOTAL_TOPICS);
	//				}
	// Create temporary corpus with fraction of docs in trng_folder




	VIRTUAL_TOTAL_DOCS = m_training_corpus->compute_virtual_num_docs();
	TOTAL_DOCS=m_training_corpus->all_docs.size();
	print_global_constants();

	//TOTAL_DOCS = fraction_training_corpus.all_docs.size();
	///////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//


	// Begin training

	vector<string> model_folder_filename = em_annotator_model_training(m_training_corpus, &m_gen, inp_settings.outer_src, output_file);
	file_h_summary<< TOTAL_DOCS <<" \t "<< model_folder_filename[0]<<endl;





	//}
	//file_h_results<<TOTAL_TOPICS<<" \t "<<TOTAL_DOCS<<" \t "<<curr_run_result[0]<<" \t "<<curr_run_result[1]<<" \t "<< curr_run_result[2]<<" \t "<<ann_rmse<<endl;

	cout << " At the end of the training! "<< endl;
	delete m_training_corpus; cout << " Deleted training corpus" << endl;
	delete m_test_corpus;cout << " Deleted test corpus" << endl;	
	file_h_summary.close();
	file_h_results.close();
	if (bins!=NULL)
		delete[] bins;
	cout << " End of code!" << endl;
}


double* read_rho_from_ann_file(string filename, int m_ann){
	if (true_rho==NULL)
		true_rho=new double[m_ann];
	fstream f_input;
	f_input.open(filename.c_str(), fstream::in);
	int total_ann_flag=0, rho_flag=0, doc_id_flag=0, classes_flag=0, rho_counter=0;
	int curr_ann=0, curr_class=0;
	while(!f_input.eof()){
		string line;f_input >> line;


		if(line.compare("TOTAL_ANN:")==0){
			total_ann_flag=1; rho_flag=0; doc_id_flag=0; classes_flag=0; continue;
		}
		if(line.compare("rho:")==0){
			total_ann_flag=0; rho_flag=1; doc_id_flag=0; classes_flag=0;
			rho_counter=0;continue;
		}
		if(line.compare("Doc_id:")==0){
			total_ann_flag=0; rho_flag=0; doc_id_flag=1; classes_flag=0;continue;
		}
		if(line.compare("Classes:")==0){
			total_ann_flag=0; rho_flag=0; doc_id_flag=0; classes_flag=1;
			curr_ann=0; curr_class=0;continue;
		}
		if (total_ann_flag==1 || doc_id_flag==1)
		{
			total_ann_flag=0; rho_flag=0; doc_id_flag=0; classes_flag=0;
			continue;
		}
		if (rho_flag==1){
			if (rho_computed==0)
				true_rho[rho_counter] = stold(line);
			rho_counter++;
			if (rho_counter== m_ann){
				total_ann_flag=0; rho_flag=0; doc_id_flag=0; classes_flag=0;
				continue;
			}
		}

	}
	rho_computed=1;
	return true_rho;
}


void convert_docs_to_slda_format(string settings_file, string data_file, int class_selected,
		string label_file, string ann_file = ""){
	corpus *m_corpus = new corpus(0);
	settings inp_settings = read_from_settings_file(settings_file);
	print_settings(inp_settings);

	string filepath; double *bins = NULL;
	if (inp_settings.svm_DirectWords==0)
	{
		bins= new double[inp_settings.svm_vocab_size];
		int idRead = 0;
		filepath=inp_settings.outer_src+ inp_settings.svm_delim_file;
		//cout<<filepath;
		fstream file_h;
		cout<<"filepath" <<filepath<<endl;
		file_h.open(filepath.c_str(), fstream::in);
		while(!file_h.eof()&&idRead!=inp_settings.svm_vocab_size){
			string line; file_h >> line;
			bins[idRead]= stod(line);
			idRead++;
		}
		//for(int i=0; i<inp_settings.vocab_size; ++i)
		//	cout<<bins[i]<<endl;
		file_h.close();
		cout<<"end of bin reads" <<endl;
	}
	TOTAL_CLASSES = inp_settings.svm_classes;
	if(inp_settings.svm_topics==0)
		TOTAL_TOPICS = TOTAL_CLASSES * 4;
	else
		TOTAL_TOPICS = TOTAL_CLASSES * inp_settings.svm_topics;
	VOCAB = inp_settings.svm_vocab_size;
	TOTAL_ANN = inp_settings.num_ann;
	filepath = inp_settings.outer_src+inp_settings.svm_trng_src;
	//read_svm_file_to_get_corpus_scene(m_training_corpus, filepath, inp_settings,bins);
	read_svm_file_to_get_corpus(m_corpus, filepath, inp_settings,bins);

	TOTAL_DOCS = m_corpus->all_docs.size();
	VIRTUAL_TOTAL_DOCS = TOTAL_DOCS;


	cout<<m_corpus->all_docs.size()<<endl;
	filepath = inp_settings.outer_src+inp_settings.svm_test_src;
	//read_svm_file_to_get_corpus_scene(m_test_corpus, filepath, inp_settings,bins);
	cout<<TOTAL_TOPICS<<endl;


	if(!ann_file.empty()){
		if (inp_settings.ann_folder.empty())
		{
			cout << " simulating annotators " << endl;
			simulate_annotators_for_corpus(m_corpus, inp_settings.outer_src, TOTAL_ANN );
		}
		else
		{
			cout << "loading annotator labels " << endl;

			load_ann_labels_from_folder(m_corpus, TOTAL_ANN, TOTAL_CLASSES,inp_settings.ann_folder);
		}
	}
	fstream data_h; data_h.open(data_file.c_str(), fstream::out);
	fstream label_h; label_h.open(label_file.c_str(), fstream::out);
	fstream ann_file_h;
	if (!ann_file.empty())
		ann_file_h.open(ann_file.c_str(), fstream::out);
	for(map<short int, Doc*>::iterator it =m_corpus->all_docs.begin(); it!= m_corpus->all_docs.end();
			it++){
		Doc* curr_doc = it->second;
		map<int, int> count_words; int distinct_words= 0;
		if (curr_doc->num_words < 25)
			continue;
		for(int i=0; i< curr_doc->num_words; i++){
			int word = curr_doc->docwords[i];
			if (count_words.find(word)!=count_words.end())
				count_words[word]++;
			else
			{
				count_words[word]=1;
				distinct_words++;
			}
		}
		data_h<<distinct_words<<" ";
		for (map<int, int>::iterator word_it=count_words.begin(); word_it!=count_words.end();
				word_it++){
			int word_feature = word_it->first;
			int word_count_val = word_it->second;
			data_h<<word_feature<<":"<<word_count_val<<" ";
		}
		data_h<<endl;
		label_h<<curr_doc->doc_classes[class_selected]<<endl;
		if (!ann_file.empty()){
			for (int i=0; i<TOTAL_ANN;i++){

				if ((curr_doc->ann_classes[i][class_selected]==0) ||
						(curr_doc->ann_classes[i][class_selected]==1))
					ann_file_h<< curr_doc->ann_classes[i][class_selected];
				else
					ann_file_h<<"-1";
				if (i!=(TOTAL_ANN-1))
					ann_file_h<<" ";

			}
		}
		ann_file_h << endl;
	}
	data_h.close();
	label_h.close();
	if (!ann_file.empty())
		ann_file_h.close();
	delete m_corpus;

}

void run_annotator_model_test(string settings_file, string output_file)
{

	settings inp_settings = read_from_settings_file(settings_file);
	print_settings(inp_settings);
	corpus *m_test_corpus = new corpus(0);
	corpus *m_training_corpus= new corpus(0);	


	int idRead = 0;
	double *bins = NULL;
	if(inp_settings.svm_DirectWords==0){
		cout << " svm_DirectWords = 0"<<endl;
		bins= new double[inp_settings.svm_vocab_size];
		string filepath=inp_settings.outer_src+ inp_settings.svm_delim_file;
		fstream file_h;
		cout<<"filepath" <<filepath<<endl;
		file_h.open(filepath.c_str(), fstream::in);
		if (!file_h.good()){
			cout  << " The file "<< filepath << " was not found! Please "
					"correct the settings file. "<< endl;
			exit(0);
		}
		while(!file_h.eof()&&idRead!=inp_settings.svm_vocab_size){
			string line; file_h >> line;
			bins[idRead]= stod(line);
			idRead++;
		}
		file_h.close();
		cout<<"End of bin reads" <<endl;

	}
	//for(int i=0; i<inp_settings.vocab_size; ++i)
	//	cout<<bins[i]<<endl;
	TOTAL_CLASSES = inp_settings.svm_classes;
	if(inp_settings.svm_topics==0)
		TOTAL_TOPICS = TOTAL_CLASSES ;
	else
		TOTAL_TOPICS = inp_settings.svm_topics;
	VOCAB = inp_settings.svm_vocab_size;
	TOTAL_ANN = inp_settings.num_ann;
	string filepath = inp_settings.outer_src+inp_settings.svm_trng_src;
	//read_svm_file_to_get_corpus_scene(m_training_corpus, filepath, inp_settings,bins);
	read_svm_file_to_get_corpus(m_training_corpus, filepath, inp_settings,bins);
	TOTAL_DOCS = m_training_corpus->all_docs.size();
	VIRTUAL_TOTAL_DOCS = TOTAL_DOCS;

	cout<<m_training_corpus->all_docs.size()<<endl;
	filepath = inp_settings.outer_src+inp_settings.svm_test_src;
	read_svm_file_to_get_corpus(m_test_corpus, filepath, inp_settings,bins);

	cout<<m_test_corpus->all_docs.size()<<endl;
	cout<<TOTAL_TOPICS<<endl;


	int doc_count =0;
	//double logl_test = log_likelihood_test(m_test_corpus, &m_gen);
	//cout << " Test log likelihood = "<< logl_test<< endl;
	fstream output_file_h(output_file);
	if (!output_file_h.good())
	{
		cout <<" The file "<< output_file << " does not exist. Please "
				"train the model first by running ./model_annotator_train <settings_file.txt>"<<endl;
		exit(0);
	}
	gen_model_params* m_gen = read_param_file(output_file);
	//cout << " m_gen loaded" << endl;
	///////Very Very important, create vector copies ///////

	for (map<short int, Doc*> ::iterator it = m_test_corpus->all_docs.begin(); it != m_test_corpus->all_docs.end();
			it++){
		m_test_corpus->all_docs_vec.push_back(it->second);
	}
	for (map<short int, Doc*> ::iterator it = m_training_corpus->all_docs.begin(); it != m_training_corpus->all_docs.end();
			it++){
		m_training_corpus->all_docs_vec.push_back(it->second);
	}

	int test_corpus_size = m_test_corpus->all_docs_vec.size();

	///    for (map<short int, Doc*> ::iterator it = m_test_corpus.all_docs.begin(); it != m_test_corpus.all_docs.end();
	//                it++){
	cout <<" Performing E test "<<test_corpus_size<<endl;
	/////////////////////////////////////
	string model_folder = inp_settings.outer_src;
	string model_folder_new = model_folder; model_folder_new.append("test/");
	mkdir(model_folder_new.c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH | S_IWOTH );

	/////////////////////////////////////
#pragma omp parallel for
	for (int doc_it = 0 ; doc_it< test_corpus_size; doc_it++){
		//Doc* curr_doc = it->second;
		Doc* curr_doc = m_test_corpus->all_docs_vec[doc_it];
		//            if (doc_count% 500 == 0)
		//                cout << " E-step "<< doc_count << " begins." << endl;

		estep_annotator_model_test(curr_doc, m_gen, model_folder_new);
		//cout << doc_it<<". " <<curr_doc->doc_id<< endl;
		//curr_doc->m_var_model_doc->print_params();
		//doc_count ++;
		//break;
	}

	//logl_test = log_likelihood_test(m_test_corpus, &m_gen, 1, model_folder);
	string test_res_folder = model_folder + "test/";
	cout << " Inference done. "<< endl;

	vector<double> curr_run_result = get_score(test_res_folder, model_folder, inp_settings.output_file);
	string ann_file = inp_settings.outer_src +"true_rho.txt";
	ifstream ann_file_h(ann_file.c_str());
	if (ann_file_h.good()){
		true_rho = read_rho_from_ann_file(ann_file, TOTAL_ANN);
		double ann_rmse = norm_diff(m_gen->rho, true_rho, TOTAL_ANN);
		cout<<"Ann RMSE :"<<ann_rmse <<endl;
	}
	cout<<"Accuracy :"<<curr_run_result[0]<<endl;


	// write settings to new file.
	string out_s_file = model_folder + "settings-model.txt";
	fstream settings_out; settings_out.open(out_s_file.c_str(), fstream::out);
	settings_out<<" TOTAL_TOPICS \t"<< TOTAL_TOPICS<< endl;
	settings_out<<" TOTAL_DOCS \t"<< TOTAL_DOCS<< endl;
	settings_out.close();
	if (bins!=NULL)
		delete[] bins;

}
