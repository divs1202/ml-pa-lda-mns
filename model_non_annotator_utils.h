/*
 *  model_non_annotator_utils.h
 *
 *  Created on: 14-Feb-2016
 *      Author: divya
 */

#ifndef MODEL_NON_ANNOTATOR_UTILS_H_
#define MODEL_NON_ANNOTATOR_UTILS_H_

#include "utils.h"
#include "model_common_utils.h"

double log_likelihood_doc_non_annotator_observed_class_compute_var_params(Doc* doc, gen_model_params* m_gen,
		int m_topics,
		int m_classes,
		int num_words,
		double ***&m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **&phi,//[num_words][TOTAL_TOPICS];
		double **&delta//[num_words][TOTAL_CLASSES];
);

void estep_non_annotator_model_test(Doc* doc, gen_model_params* m_gen, string model_folder  );
void estep_non_annotator_model_training(corpus* m_corpus, gen_model_params* m_gen, int outer = 0);
void estep_non_annotator_model_training_opt(corpus* m_corpus, gen_model_params* m_gen, int outer = 0);

void mstep_non_annotator_model_training(corpus* m_corpus, gen_model_params* m_gen);
vector<string >em_non_annotator_model_training(corpus* m_corpus, gen_model_params* m_gen,  
string src_folder="/home/divya/workspace/multiLabelCrowd/src/" );
void run_non_annotator_model_train(string settings_file, string output_file);
void run_non_annotator_model_test(string settings_file, string output_file);
double log_likelihood_doc_test(Doc* doc, gen_model_params* m_gen,
		int m_topics,
		int m_classes,
		int num_words,
		double *&c_delta,//[TOTAL_CLASSES];
		double ***&m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **&phi,//[num_words][TOTAL_TOPICS];
		double **&delta//[num_words][TOTAL_CLASSES];
);

#endif /*MODEL_NON_ANNOTATOR_UTILS_H_*/
