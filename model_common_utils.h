/*
 *  model_common_utils.h
 *
 *  Created on: 14-Feb-2016
 *      Author: divya
 */

#ifndef MODEL_COMMON_UTILS_H
#define MODEL_COMMON_UTILS_H


#include "utils.h"
#include "corpus.h"
#include "gen_model.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_sf_psi.h>
#include <math.h>
#include <string.h>
#include <sstream>
#include <sys/time.h>
#include <sys/stat.h>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>
extern int VOCAB;
extern int TOTAL_CLASSES;
extern int TOTAL_DOCS;
extern int TOTAL_TOPICS;
extern int TOTAL_ANN;
extern int MAX_E_ITER_TEST;
extern int MAX_EM_ITER;
extern int MAX_NR_ITERATIONS;
extern int MAX_E_ITER_TRNG;

typedef unsigned long long timestamp_t;

extern int VIRTUAL_TOTAL_DOCS;
extern double TOLERANCE;
extern double PER_DOC_TOLERANCE;
extern vector<string> class_list;
extern vector<string> vocab_list;
extern vector<vector<short int>> ar_orig_docs;
extern map<int, vector<short int>> labels_by_ann;
extern int *class_size;
extern double *true_rho;
extern int rho_computed;
// sufficient statistics
extern double *suff1; //sum of c_delta_i
extern double *suff2; // annotator terms sum
extern int *suff3; //no. of labels given by each annotator
extern double **suff4; //beta numerator term
extern double ***suff5; // digamma_sum term, used in alpha opt

void set_global_constants(string training_dir, string dir, string vocab_file="final_vocab.txt", string class_file="final_class.txt");
void print_global_constants();
void reset_params(int m_total_topics, int m_total_classes,int num_words,
		double *&c_delta,//[TOTAL_CLASSES];
		double ***&m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **&phi,//[num_words][TOTAL_TOPICS];
		double **&delta//[num_words][TOTAL_CLASSES];
);
void delete_params(int m_total_topics, int m_total_classes,int num_words,
		double *&c_delta,//[TOTAL_CLASSES];
		double ***&m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **&phi,//[num_words][TOTAL_TOPICS];
		double **&delta//[num_words][TOTAL_CLASSES];
);
map<short int, Doc*> form_docid_doc_map(string dir_name);

void initialize_suff(int total_classes, int total_topics, int vocab, int total_ann=0);
void create_and_initialize_suff(int total_classes, int total_topics, int vocab, int total_ann=0);
void delete_suff(int total_classes, int total_topics, int vocab, int total_ann=0);

struct settings{
	string model_folder, param_file,
	test_res_folder, output_file, outer_src, ann_folder, dataset_name;
	int num_ann;
	string svm_trng_src, svm_test_src;
	int svm_vocab_size;
	string svm_delim_file;
	int svm_classes;
	int svm_topics;
	int svm_DirectWords;
};
void print_settings(settings m_settings);
void read_svm_file_to_get_corpus(corpus *m_corpus,string filepath,settings settings, double *bins);
settings read_from_settings_file(string settings_file);
gen_model_params* read_param_file(string filename);
vector<double> get_score(string dirname, string outer_src, string file_name="results.txt");
static timestamp_t get_timestamp()
{
	struct timeval now;
	gettimeofday (&now, NULL);
	return  now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}
double log_likelihood_doc_test(Doc* doc, gen_model_params* m_gen,
		int m_topics,
		int m_classes,
		int num_words,
		double *&c_delta,//[TOTAL_CLASSES];
		double ***&m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **&phi,//[num_words][TOTAL_TOPICS];
		double **&delta//[num_words][TOTAL_CLASSES];
);
#endif /* MODEL_COMMON_UTILS_H */
