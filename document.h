/*
 *  document.h
 *
 *  Created on: 14-Feb-2016
 *      Author: divya
 */

#ifndef DOCUMENT_H_
#define DOCUMENT_H_
#include "utils.h"

class Doc{
public:
	vector<short int> docwords;
	double weightage;
	short int doc_id;
	int num_words,m_total_classes, m_total_ann;
	int *doc_classes;//[TOTAL_CLASSES];
	int **ann_classes;//[TOTAL_ANN][TOTAL_CLASSES];	
	int ann_present_flag, class_observed_flag;
	double log_l;

	Doc(short int docu_id, int total_classes, int total_ann, int total_topics,
			vector<short int> docu_words, int* docu_classes, int class_observed_arg = 1,
			int** anno_classes = NULL){
		weightage = 1;
		doc_id = docu_id;
		m_total_classes = total_classes; m_total_ann= total_ann;
		doc_classes = new int[total_classes];
		if (total_ann==0)
			ann_classes = NULL;
		else
			ann_classes = new int*[total_ann];
		class_observed_flag = class_observed_arg;
		ann_present_flag =0;
		for (int i=0; i< total_ann; i++)
		{
			ann_classes[i] = new int[total_classes];
			if (anno_classes==NULL)
				ann_present_flag=0;
			else
			{
				ann_present_flag = 1;
				for (int j=0; j<total_classes; j++){
					ann_classes[i][j] = anno_classes[i][j];
				}
			}
		}
		docwords.insert(docwords.begin(), docu_words.begin(), docu_words.end());		
		num_words = docu_words.size();
		for (int i=0; i<total_classes; i++)
			doc_classes[i] = docu_classes[i];

	}
	void print_doc_contents(){
		cout<< " Doc id = "<< doc_id<< endl;
		cout<<" Num_words = " << num_words<< endl;
		cout<< " Class vector = ";
		for(int i= 0; i< m_total_classes; i++)
			cout<<doc_classes[i]<<" ";
		cout<<" \n Words = ";
		for (int i=0; i<docwords.size(); i++)
			cout<< docwords[i] <<" ";
		cout<<endl;
		for(int i= 0; i< m_total_ann; i++)
		{
			for(int j=0; j<m_total_classes; j++)
				cout<<ann_classes[i][j]<<" ";
			cout<<endl;
		}
		return;
	}
	~Doc(){
		delete[] doc_classes;
		for (int i=0; i< m_total_ann; i++)
			delete[] ann_classes[i];
		delete[] ann_classes;
	}	
};
#endif /*DOCUMENT_H_*/

