CC=g++
CFLAGS=-std=c++11 -c -Wall -O3 -Wno-sign-compare -Wno-unused-parameter -Wno-unused-variable -Wno-unused-function -Wno-unused-but-set-variable -Wno-maybe-uninitialized -fopenmp
LDFLAGS=-lgsl -lgslcblas -lgomp

all: model_non_annotator_train model_non_annotator_test model_annotator_train model_annotator_test

utils.o: utils.cpp utils.h
	$(CC) $(CFLAGS) -O3 -c utils.cpp -o utils.o

model_common_utils.o: utils.o model_common_utils.cpp model_common_utils.h document.h corpus.h gen_model.h
	$(CC) $(CFLAGS) -O3 -c model_common_utils.cpp -o model_common_utils.o

model_non_annotator_utils.o: model_common_utils.o model_non_annotator_utils.cpp model_non_annotator_utils.h
	$(CC) $(CFLAGS) -O3 -c model_non_annotator_utils.cpp -o model_non_annotator_utils.o
	
model_non_annotator_train.o: model_non_annotator_utils.o model_non_annotator_train.cpp
	$(CC) $(CFLAGS) -O3 -c model_non_annotator_train.cpp -o model_non_annotator_train.o

model_non_annotator_test.o: model_non_annotator_utils.o model_non_annotator_test.cpp
	$(CC) $(CFLAGS) -O3 -c model_non_annotator_test.cpp -o model_non_annotator_test.o
	
model_non_annotator_train: model_non_annotator_train.o model_non_annotator_utils.o model_common_utils.o utils.o
	$(CC) -O3 $^ $(LDFLAGS) -o $@
	
model_non_annotator_test: model_non_annotator_test.o model_non_annotator_utils.o model_common_utils.o utils.o
	$(CC) -O3 $^ $(LDFLAGS) -o $@

model_annotator_utils.o: model_common_utils.o model_annotator_utils.cpp model_annotator_utils.h
	$(CC) $(CFLAGS) -O3 -c model_annotator_utils.cpp -o model_annotator_utils.o
	
model_annotator_train.o: model_annotator_utils.o model_annotator_train.cpp
	$(CC) $(CFLAGS) -O3 -c model_annotator_train.cpp -o model_annotator_train.o
	
model_annotator_train: model_annotator_train.o model_annotator_utils.o model_common_utils.o utils.o
	$(CC) -O3 $^ $(LDFLAGS) -o $@

model_annotator_test.o: model_annotator_utils.o model_annotator_test.cpp
	$(CC) $(CFLAGS) -O3 -c model_annotator_test.cpp -o model_annotator_test.o
	
model_annotator_test: model_annotator_test.o model_annotator_utils.o model_common_utils.o utils.o
	$(CC) -O3 $^ $(LDFLAGS) -o $@


.PHONY: clean
clean:
	rm -rf *.o
    
