/*
 * utils.h
 *
 *  Created on: 14-Feb-2016
 *      Author: divya
 */

#ifndef UTILS_H_
#define UTILS_H_
#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include <algorithm>
#include <map>
#include <set>
#include <dirent.h>
#include <stdio.h>
#include <cmath>
#include <sys/time.h>
#include <sys/stat.h>
using namespace std;

set<string> populate_word_list(string filename);
void write_final_vocab_to_file(string dir);
void write_final_topic_to_file(string dir, int max_topics);
vector<string> read_directory(string path);
vector<string> ls_command(string path);
void phase5_processing();
void form_splits_to_training_and_test(string input_dir, double training_percent);
#endif /* UTILS_H_ */
