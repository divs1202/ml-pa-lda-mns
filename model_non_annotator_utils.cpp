/*
 *  model_non_annotator_utils.cpp
 *
 *  Created on: 14-Feb-2016
 *      Author: divya
 */
#include "model_common_utils.h"
#include "model_non_annotator_utils.h"

// sufficient statistics
double *suff1; //sum of c_delta_i
double *suff2; // annotator terms sum
int *suff3; //no. of labels given by each annotator
double **suff4; //beta numerator term
double ***suff5; // digamma_sum term, used in alpha opt

int VOCAB;
int TOTAL_CLASSES;
int TOTAL_DOCS;
int TOTAL_TOPICS;
int TOTAL_ANN = 0;
int MAX_E_ITER_TEST = 100;
int MAX_EM_ITER = 150;
int MAX_NR_ITERATIONS = 100;
int MAX_E_ITER_TRNG = 100;

int VIRTUAL_TOTAL_DOCS;
double TOLERANCE = 1e-4;
double PER_DOC_TOLERANCE= 1e-2;

vector<string> class_list;
vector<string> vocab_list;
vector<vector<short int>> ar_orig_docs;
map<int, vector<short int>> labels_by_ann;
int *class_size;
double *true_rho;
int rho_computed = 0;


double log_likelihood_doc_non_annotator_observed_class_compute_var_params(Doc* doc, gen_model_params* m_gen,
		int m_topics,
		int m_classes,
		int num_words,
		double ***&m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **&phi,//[num_words][TOTAL_TOPICS];
		double **&delta//[num_words][TOTAL_CLASSES];
){
	double sum_gamma[TOTAL_CLASSES][2], digamma_f_gamma[TOTAL_CLASSES][2][TOTAL_TOPICS];
	double sum_alpha[TOTAL_CLASSES][2], digamma_f_alpha[TOTAL_CLASSES][2][TOTAL_TOPICS];
	//double sum_chi[TOTAL_TOPICS], digamma_f_chi[TOTAL_TOPICS][VOCAB];
	//double sum_eta[TOTAL_TOPICS], digamma_f_eta[TOTAL_TOPICS][VOCAB];

	// populate psi functions for gamma and alpha
	for (int i=0; i< TOTAL_CLASSES; i++){
		for (int j=0; j<=1; j++){
			sum_gamma[i][j] = 0; sum_alpha[i][j]=0;
			for (int t=0; t< TOTAL_TOPICS; t++){
				sum_gamma[i][j] += m_gamma[i][j][t];
				digamma_f_gamma[i][j][t] = gsl_sf_psi(m_gamma[i][j][t]);
				sum_alpha[i][j] += m_gen->alpha[i][j][t];
				digamma_f_alpha[i][j][t] = gsl_sf_psi(m_gen->alpha[i][j][t]);

			}

			for (int t=0; t<TOTAL_TOPICS; t++){
				digamma_f_gamma[i][j][t] -= gsl_sf_psi(sum_gamma[i][j]);
				digamma_f_alpha[i][j][t] -= gsl_sf_psi(sum_alpha[i][j]);
			}
		}
	}
	//cout << " Computed psi for gamma and alpha"<< endl;
	// populate psi functions for chi and eta

	//cout << " Computed psi for chi  and eta"<< endl;
	double elbo = 0;
	for (int i=0; i< TOTAL_CLASSES; i++){
		elbo += (doc->doc_classes[i]*log(max(m_gen->xi[i],1e-6 )))+
				((1-doc->doc_classes[i])*log(max(1-m_gen->xi[i],1e-6)));
		for (int j=0; j<=1; j++){
			//cout<<sum_alpha[i][j]<<" "<<sum_gamma[i][j]<<endl;

			elbo+= gsl_sf_lngamma(sum_alpha[i][j]) - gsl_sf_lngamma(sum_gamma[i][j]);

			for (int t=0; t< TOTAL_TOPICS; t++){
				elbo = elbo + gsl_sf_lngamma(m_gamma[i][j][t])
																																		- gsl_sf_lngamma(m_gen->alpha[i][j][t]) + ((m_gen->alpha[i][j][t] -
																																				m_gamma[i][j][t])*digamma_f_gamma[i][j][t]);

				for(int n=0; n< doc->num_words; n++){
					elbo+= (doc->doc_classes[i]==j)*delta[n][i]
															 * phi[n][t]* digamma_f_gamma[i][j][t];
				}
			}
		}
		//	cout << i<<". Finished i-j-t-n. Now out of j loop" << endl;
		for (int n=0; n<doc->num_words; n++){
			elbo += delta[n][i]*(log(m_gen->kappa[i])
					- log(max(delta[n][i],1e-6)));
		}


	}
	//cout << " Finished i-j-t-n. Now out of i loop" << endl;
	for (int t=0; t<TOTAL_TOPICS; t++){
		//cout << "sum_eta:" << sum_eta[t] <<" , sum_chi: "<< sum_chi[t]<<endl;
		//	elbo+= gsl_sf_lngamma(sum_eta[t]) - gsl_sf_lngamma(sum_chi[t]);
		//cout << " Entering j loop" << endl;
		for (int n=0; n<doc->num_words; n++){
			int j=doc->docwords[n];
			elbo+= (phi[n][t]* (log(max(m_gen->beta[t][j],1e-6))
					- log(max(phi[n][t],1e-6))));
		}
	}
	return elbo;
}

void estep_non_annotator_model_test(Doc* doc, gen_model_params* m_gen, string model_folder  ){
	double sum_gamma[TOTAL_CLASSES][2], digamma_f_gamma[TOTAL_CLASSES][2][TOTAL_TOPICS];
	double sum_alpha[TOTAL_CLASSES][2], digamma_f_alpha[TOTAL_CLASSES][2][TOTAL_TOPICS];

	double *c_delta;//[TOTAL_CLASSES];
	double ***m_gamma;//[TOTAL_CLASSES][2][TOTAL_TOPICS];
	double **phi;//[num_words][TOTAL_TOPICS];
	double **delta;
	double log_l_doc;

	reset_params(TOTAL_TOPICS, TOTAL_CLASSES, doc->num_words,
			c_delta,//[TOTAL_CLASSES];
			m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
			phi,//[num_words][TOTAL_TOPICS];
			delta//[num_words][TOTAL_CLASSES];
	);

	//double log_l_old = log_likelihood_doc_annotator_observed_class_new(doc, m_gen);
	double log_l_old =log_likelihood_doc_test(
			doc,
			m_gen,
			TOTAL_TOPICS,
			TOTAL_CLASSES,
			doc->num_words,
			c_delta,//[TOTAL_CLASSES];
			m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
			phi,//[num_words][TOTAL_TOPICS];
			delta//[num_words][TOTAL_CLASSES];
	);



	// populate psi functions for alpha
	for (int i=0; i< TOTAL_CLASSES; i++){
		for (int j=0; j<=1; j++){
			sum_gamma[i][j] = 0; sum_alpha[i][j]=0;
			for (int t=0; t< TOTAL_TOPICS; t++){
				sum_alpha[i][j] += m_gen->alpha[i][j][t];
				digamma_f_alpha[i][j][t] = gsl_sf_psi(m_gen->alpha[i][j][t]);

			}

			for (int t=0; t<TOTAL_TOPICS; t++){
				digamma_f_alpha[i][j][t] -= gsl_sf_psi(sum_alpha[i][j]);
			}
		}
	}

	int converged =0; int num_iter = 0;
	double log_l=0; double log_l_init=0;

	while (converged ==0 && num_iter < MAX_E_ITER_TEST ){
		// Estep for loop. till convergence.
		//populate digamma function for gamma.
		for (int i=0; i< TOTAL_CLASSES; i++){
			for (int j=0; j<=1; j++){
				sum_gamma[i][j] = 0;
				for (int t=0; t< TOTAL_TOPICS; t++){
					sum_gamma[i][j] += m_gamma[i][j][t];
					digamma_f_gamma[i][j][t] = gsl_sf_psi(m_gamma[i][j][t]);

				}

				for (int t=0; t<TOTAL_TOPICS; t++){
					digamma_f_gamma[i][j][t] -= gsl_sf_psi(sum_gamma[i][j]);
					//	cout <<" Digamma "<<i <<","<<j<<", "<<t<<" = "<<digamma_f_gamma[i][j][t]<<endl;
				}
			}
		}
		for (int i= 0; i< TOTAL_CLASSES; i++){
			double a = log(max(m_gen->xi[i],1e-6)), b=  log(max(1- m_gen->xi[i],1e-6));
			//cout <<" i = "<< i<<" a = "<< a<< " b = "<< b<< endl;
			//cout<<" -------------------------------------------- "<<endl;
			for (int t=0; t<TOTAL_TOPICS; t++){
				for (int n= 0; n< doc->num_words; n++){
					a = a + (delta[n][i]* phi[n][t]*
							digamma_f_gamma[i][1][t]);
					b= b+ (delta[n][i]* phi[n][t]*
							digamma_f_gamma[i][0][t]);
					//cout <<" n = "<< n<< " a = "<< a <<" b = "<<b<<endl;
				}
			}
			c_delta[i] = 1.0/(1+ exp(b-a));
			//cout<<num_iter<<". a= "<< a<< " b= "<< b<<" "<<
			// c_delta[i]<<" " <<doc->doc_classes[i]<< endl;


		}
		// compute delta

		for (int n=0; n<doc->num_words; n++){
			double *temp = new double[TOTAL_CLASSES];
			for (int i=0; i< TOTAL_CLASSES; i++)
				temp[i] =m_gen->kappa[i];//1.0/TOTAL_CLASSES;

			for (int i=0; i<TOTAL_CLASSES; i++){
				for (int j=0; j<=1; j++){
					for (int t=0; t<TOTAL_TOPICS; t++){
						temp[i] += pow(c_delta[i],j) *
								pow(1-c_delta[i], 1-j) *
								phi[n][t] * digamma_f_gamma[i][j][t];
					}
				}

			}

			for (int i=0; i<TOTAL_CLASSES; i++){
				double total_val = 0;
				for (int j=0; j<TOTAL_CLASSES; j++){
					total_val += exp(temp[j]- temp[i]);
				}
				delta[n][i] = 1.0/(total_val);
				delta[n][i] = 1.0/TOTAL_CLASSES;
				//cout<<" delta[ "<<n<<" ]["<<i<<"] = "<<delta[n][i] << endl;
			}

			delete[] temp;
		}

		// compute phi
		for (int n=0; n<doc->num_words; n++){
			double *temp = new double[TOTAL_TOPICS];
			int j =doc->docwords[n];

			for (int t=0; t<TOTAL_TOPICS; t++){
				temp[t] = log(max(m_gen->beta[t][j],1e-6));
				for (int i=0; i<TOTAL_CLASSES; i++){
					temp[t] += c_delta[i] * delta[n][i] *
							digamma_f_gamma[i][1][t];
					temp[t] += (1-c_delta[i]) * delta[n][i] *
							digamma_f_gamma[i][0][t];

				}
			}
			for (int t=0; t<TOTAL_TOPICS; t++){
				double total_val = 0;
				for (int j=0; j<TOTAL_TOPICS; j++){
					total_val += exp(temp[j]- temp[t]);
				}
				phi[n][t] = 1.0/(total_val);
			}
			delete[] temp;
		}

		// compute gamma
		for (int i=0; i< TOTAL_CLASSES; i++){
			for (int j=0; j<=1; j++){
				for (int t=0; t<TOTAL_TOPICS; t++){
					m_gamma[i][j][t] = m_gen->alpha[i][j][t];
					for (int n=0; n<doc->num_words; n++){
						m_gamma[i][j][t] +=
								pow(c_delta[i], j)*
								pow(1-c_delta[i],1- j) *
								delta[n][i] * phi[n][t];
					}
				}
			}
		}

		/*for (int t=0; t<TOTAL_CLASSES; t++){
			for (int j=0 ; j< VOCAB; j++){
				m_complete_var->chi[t][j] = m_gen->eta[t][j];
			}
			for (int n=0; n< doc->num_words; n++){
				int j= doc->docwords[n];
				m_complete_var->chi[t][j] += phi[n][t];


			}

		}*/

		log_l_init= log_l;
		log_l=  log_likelihood_doc_test(
				doc,
				m_gen,
				TOTAL_TOPICS,
				TOTAL_CLASSES,
				doc->num_words,
				c_delta,//[TOTAL_CLASSES];
				m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
				phi,//[num_words][TOTAL_TOPICS];
				delta//[num_words][TOTAL_CLASSES];
		);
		if(num_iter !=0 )
			converged = (fabs((log_l-log_l_init)/log_l_init) < TOLERANCE);
		num_iter++;
	}

	string test_result_filename = model_folder + to_string(doc->doc_id);
	//test_result_filename.append(to_string(doc_id));
	test_result_filename = test_result_filename + ".txt";
	fstream test_result;

	test_result.open(test_result_filename.c_str(), fstream::out);
	test_result<< doc->doc_id<< " "<<endl;


	for (int i = 0; i< TOTAL_CLASSES; i++)
	{
		test_result<< doc->doc_classes[i]<<" "<< c_delta[i]<<endl;
	}


	test_result.close();
	//cout<<test_result<<endl;

	delete_params(TOTAL_TOPICS, TOTAL_CLASSES, doc->num_words,
			c_delta,//[TOTAL_CLASSES];
			m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
			phi,//[num_words][TOTAL_TOPICS];
			delta//[num_words][TOTAL_CLASSES];
	);


	//cout<< " Converged in "<< num_iter<<" iterations"<<endl;
}

void estep_non_annotator_model_training(corpus* m_corpus, gen_model_params* m_gen, int outer /*= 0*/){
	timestamp_t t0 = get_timestamp(); double log_l= 0;
	//    double log_l_corpus =log_likelihood_complete_observed_class(m_corpus,
	//            m_gen, m_complete_var, 1);
	double log_l_corpus_old =m_corpus->log_l;
	//    cout << " E"<<" starting with logl "<< log_l_corpus<<endl;
	int total_num_docs = m_corpus->all_docs.size();
	initialize_suff(TOTAL_CLASSES, TOTAL_TOPICS, VOCAB, TOTAL_ANN);
#pragma omp parallel for
	for (int doc_it = 0; doc_it <total_num_docs; doc_it++)
	{
		//short int doc_id = it->first;

		Doc* doc = m_corpus->all_docs_vec[doc_it];
		//temp variables
		double *c_delta;//[TOTAL_CLASSES];
		double ***m_gamma;//[TOTAL_CLASSES][2][TOTAL_TOPICS];
		double **phi;//[num_words][TOTAL_TOPICS];
		double **delta;
		double log_l_doc;


		//double log_l_old = log_likelihood_doc_annotator_observed_class_new(doc, m_gen);
		double log_l_old = doc->log_l;
		//cout<<TOTAL_TOPICS<<" "<< TOTAL_CLASSES<<" " <<doc->num_words<<endl;
		reset_params(TOTAL_TOPICS, TOTAL_CLASSES, doc->num_words,
				c_delta,//[TOTAL_CLASSES];
				m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
				phi,//[num_words][TOTAL_TOPICS];
				delta//[num_words][TOTAL_CLASSES];
		);
		//doc->m_var_model_doc->reset_params(TOTAL_TOPICS);
		// store the digamma function values of gamma
		// removing m_var_model_doc
		double digamma_temp[TOTAL_CLASSES][2][TOTAL_TOPICS];
		for(int innerEstep = 0; innerEstep < MAX_E_ITER_TRNG ; ++innerEstep)  //for E Step
		{
			// Initialize all variational params. --done

			int doc_count = 0;
			for (int i=0; i< TOTAL_CLASSES; i++){
				for (int j=0; j<=1; j++){
					double digamma_sum= 0;
					for (int t=0; t<TOTAL_TOPICS; t++)
					{
						digamma_temp[i][j][t] = 0;
						digamma_sum += m_gamma[i][j][t];
					}
					digamma_sum = gsl_sf_psi(digamma_sum);
					for (int t=0; t<TOTAL_TOPICS; t++){
						digamma_temp[i][j][t] = gsl_sf_psi(m_gamma[i][j][t]) - digamma_sum;
					}
				}
			}

			//   phi_n_t updates
			for (int n=0; n< doc->num_words; n++){

				double temp [ TOTAL_TOPICS] , total[TOTAL_TOPICS];

				//double digamma_sum_1[TOTAL_CLASSES] , digamma_sum_0[TOTAL_CLASSES];
				//for (int i=0; i< TOTAL_CLASSES; i++){
				//    digamma_sum_1[i] = 0;
				for (int t=0; t< TOTAL_TOPICS; t++){
					temp[t] = 0;
					//    digamma_sum_1[i]+= doc->m_var_model_doc->m_gamma[i][1][t];
					//    digamma_sum_0[i]+= doc->m_var_model_doc->m_gamma[i][0][t];
				}
				//}
				for (int t=0; t< TOTAL_TOPICS; t++){

					for (int i=0; i< TOTAL_CLASSES; i++){

						temp[t] += delta[n][i] *((doc->doc_classes[i] * (digamma_temp[i][1][t]))//gsl_sf_psi(doc->m_var_model_doc->m_gamma[i][1][t] - gsl_sf_psi(digamma_sum_1[i]))))
								+ ((1-doc->doc_classes[i])    * digamma_temp[i][0][t]));
					}
					int j= doc->docwords[n];
					temp[t] += log(max(m_gen->beta[t][j],1e-6));
					//(gsl_sf_psi(m_complete_var.chi[t][doc->docwords[n]]) - gsl_sf_psi(chi_sum));


				}
				int sum_phi = 0;
				for (int t=0; t< TOTAL_TOPICS; t++){
					{
						total[t] =0;
						for (int t1 = 0; t1< TOTAL_TOPICS; t1++)
							total[t] += exp(temp[t1]- temp[t]);
					}
					phi[n][t] = 1.0/(total[t]);
					sum_phi += phi[n][t];
				}
				//static_assert(sum_phi==1, " Sum_phi = " << sum_phi);
			}

			//   delta_n_i updates
			for (int n=0; n< doc->num_words; n++){
				for(int i=0; i< TOTAL_CLASSES; i++){
					delta[n][i] = 1.0/TOTAL_CLASSES;
				}
			}


			//   gamma_i_j_t
			for(int i=0; i< TOTAL_CLASSES; i++){
				for (int t=0; t<TOTAL_TOPICS; t++)
				{
					m_gamma[i][0][t] = m_gen->alpha[i][0][t];
					m_gamma[i][1][t] = m_gen->alpha[i][1][t];
					for (int n=0; n< doc->num_words; n++){


						m_gamma[i][1][t]+=(doc->doc_classes[i])*
								delta[n][i]*
								phi[n][t];

						m_gamma[i][0][t]+=(1-doc->doc_classes[i])*
								delta[n][i]*
								phi[n][t];


					}
				}

			}



			//break;



			/*    if (outer%5==4){
//collapse(2)
        //separate loop for chi updates, chi_t_j updates
        int total_num_docs = m_corpus->all_docs_vec.size();

//#pragma omp parallel for //collapse(2)
        for (int t=0; t< TOTAL_TOPICS; t++){
            for(int j=0; j< VOCAB; j++){
                m_complete_var->chi[t][j] = m_gen->eta[t][j];
            }
                for(int doc_it=0; doc_it< total_num_docs; doc_it++){
                    Doc* doc= m_corpus->all_docs_vec[doc_it];
                    for (int n=0; n< doc->num_words; n++){
                        int j = doc->docwords[n];
                        //if (j== doc->docwords[n]){
                        m_complete_var->chi[t][j] += doc->m_var_model_doc->phi[n][t];
                        //}

                    }
                }

            //}

        }*/
			//cout << " Updated chi "<<endl;
			/*    for(int t=0; t<TOTAL_TOPICS; t++){
                    cout<<" New topic "<< t<< ":"<<endl;
                for(int j=0; j< VOCAB; j++){
                                    cout<<m_complete_var->chi[t][j]<<endl;
                                }

                }*/
			//}

			//break;
			//            log_l =log_likelihood_complete_observed_class(m_corpus,
			//                    m_gen, m_complete_var, 1);
			log_l_doc =
					log_likelihood_doc_non_annotator_observed_class_compute_var_params(
							doc,
							m_gen,
							TOTAL_TOPICS,
							TOTAL_CLASSES,
							doc->num_words,
							m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
							phi,//[num_words][TOTAL_TOPICS];
							delta//[num_words][TOTAL_CLASSES];
					);

			//           double rel_diff = fabs((log_l - log_l_old)/log_l_corpus);
			//           rel_diff = rel_diff * TOTAL_DOCS;
			double rel_diff = fabs((log_l_doc - log_l_old)/log_l_old);
			doc->log_l= log_l_doc;
			//cout<<" Inner E-logl =" <<log_l<<" Rel_diff = "<<rel_diff<<endl;

			if(outer == 0 && innerEstep >=10)
				break;
			else if (rel_diff< PER_DOC_TOLERANCE )
				break;
		}
		//cout<< doc_it<<" done!" <<endl;
#pragma omp critical
		{
			log_l += (log_l_doc* doc->weightage);
			for (int i=0; i<TOTAL_CLASSES; i++)
			{
				suff1[i] = suff1[i] + (doc->doc_classes[i]*doc->weightage/VIRTUAL_TOTAL_DOCS);
			}
			for (int n=0; n<doc->num_words; n++){
				int j=doc->docwords[n];

				for (int t=0; t<TOTAL_TOPICS; t++)
					suff4[t][j] += phi[n][t];
			}

			double digamma_temp[TOTAL_CLASSES][2];
			for (int i=0; i<TOTAL_CLASSES; i++){
				digamma_temp[i][0] = 0;
				digamma_temp[i][1] = 0;
				for(int t=0; t<TOTAL_TOPICS; t++){
					digamma_temp[i][0] += m_gamma[i][0][t];
					digamma_temp[i][1] += m_gamma[i][1][t];

				}

				for (int t=0; t<TOTAL_TOPICS; t++){
					suff5[i][0][t] += gsl_sf_psi( m_gamma[i][0][t])
    													- gsl_sf_psi(digamma_temp[i][0]);
					suff5[i][1][t] += gsl_sf_psi( m_gamma[i][1][t])
        													- gsl_sf_psi(digamma_temp[i][1]);
				}
			}

		}
		delete_params(TOTAL_TOPICS, TOTAL_CLASSES, doc->num_words,
				c_delta,//[TOTAL_CLASSES];
				m_gamma,//[TOTAL_CLASSES][2][TOTAL_TOPICS];
				phi,//[num_words][TOTAL_TOPICS];
				delta//[num_words][TOTAL_CLASSES];
		);

	} // for every doc
	// compute log likelihood;
	log_l = log_l*1.0/m_corpus->compute_virtual_num_docs();
	cout<<" E-log_l_old"<< log_l_corpus_old <<" logl_new =" <<log_l << endl;
	m_corpus->log_l = log_l;
	timestamp_t t1 = get_timestamp();
	double secinner = (t1 - t0) / 1000000.0L;
	cout<< " Time for E = "<< secinner<< endl;
}

void mstep_non_annotator_model_training(corpus* m_corpus, gen_model_params* m_gen){
	long seed = time(NULL);
	gsl_rng *r_generator = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r_generator, seed);
	timestamp_t t1 = get_timestamp();

	// Initialize xi, alpha, eta, rho (For annotator model)
	/*for (int i=0; i<TOTAL_CLASSES; i++){
		for (int j=0; j<=1; j++){
			for (int t=0; t<TOTAL_TOPICS; t++)
				m_gen->alpha[i][j][t] = gsl_ran_gamma(r_generator, 1,1);
		}
	}
	for (int t=0; t<TOTAL_TOPICS; t++){
		for (int j=0; j<VOCAB; j++){
			m_gen->eta[t][j] = gsl_ran_beta(r_generator, 1,1);
		}
	}*/
	//M Step starts
	// xi_i updates
	// rho_i updates
	// Newton Rhapson updates for alpha_i_j_r
	// Newton Rhapson updates for eta_i_j
	//M Step starts
	// xi_i updates
#pragma omp parallel for
	for(int i=0; i< TOTAL_CLASSES; i++)
	{
		m_gen->xi[i]=suff1[i];
	}

	int break_from_nr_alpha = 0;
#pragma omp parallel for collapse(2)
	for (int i=0; i<TOTAL_CLASSES; ++i)
	{
		for(int j=0; j<2; j++)
		{
			for(int nr_for_alpha = 0; nr_for_alpha < MAX_NR_ITERATIONS; ++nr_for_alpha)
			{
				double sum_all = 0.0;
				for(int tIdx = 0; tIdx < TOTAL_TOPICS; ++tIdx){
					sum_all += m_gen->alpha[i][j][tIdx];
				}
				// sum_all = sum(m_gen.alpha[i][j][.]);
				double z = VIRTUAL_TOTAL_DOCS * (gsl_sf_psi_n(1,sum_all));
				double g[TOTAL_TOPICS], h[TOTAL_TOPICS], c_denom = 1.0/z, c_num=0.0, c, sum_gr=0;
				//#pragma omp parallel for reduction (+: c_num,c_denom, sum_gr)
				for(int r=0; r<TOTAL_TOPICS; ++r)
				{
					//cout<<suff5[j][j][r]<<endl;
					g[r] = suff5[i][j][r];
					g[r] += VIRTUAL_TOTAL_DOCS *(gsl_sf_psi_n(0,sum_all)- gsl_sf_psi_n(0, m_gen->alpha[i][j][r]));

					h[r] = -VIRTUAL_TOTAL_DOCS * gsl_sf_psi_n(1, m_gen->alpha[i][j][r]);
					c_denom = c_denom + 1/h[r];
					c_num += (g[r]/h[r]);
					sum_gr += pow(g[r],2);
				}
				sum_gr = sqrt(sum_gr);
				c = c_num/c_denom;
				if (sum_gr< TOLERANCE)
					break;
				double old_r_val[TOTAL_TOPICS];
				for(int r=0; r<TOTAL_TOPICS; ++r)
					old_r_val[r] = m_gen->alpha[i][j][r];
				int reset_old= 0;
				//#pragma omp parallel for
				for(int r=0; r<TOTAL_TOPICS; ++r)
				{

					m_gen->alpha[i][j][r] -= (g[r]-c)/h[r];
					if (m_gen->alpha[i][j][r]<=0 )
					{
						reset_old = 1;
						cout << " Resetting alpha to 1e-6:" <<i <<", "<<j<<" , " <<r<<endl;
						m_gen->alpha[i][j][r] = 1e-6;// old_val;
						//	break_from_nr_alpha = 1;
					}
					if(std::isnan(m_gen->alpha[i][j][r]))
						cout<<"got "<<VIRTUAL_TOTAL_DOCS<<endl; //<<i<" "<<j<<" "<<r<<endl;
				}
				/*if (reset_old == 1){
						//cout << " Resetting  alpha "<< endl;
						for(int r=0; r<TOTAL_TOPICS; ++r)
							m_gen->alpha[i][j][r]= old_r_val[r];
						break;
					}
				 */
				//cout << " sum_ gr = "<<sum_gr<<endl;

			}
		}
		//break;
		//if (break_from_nr_alpha == 1)
		//	break;
	}

	//	cout << " NR for alpha done! "<< endl;

	// Updates for beta
	int total_num_docs = m_corpus->all_docs_vec.size();
	for (int t=0; t<TOTAL_TOPICS; t++){
		double temp = 0;
		for (int j=0; j<VOCAB; j++){
			m_gen->beta[t][j] = (1.0/(VOCAB*VOCAB*VOCAB)) + suff4[t][j];
			temp += m_gen->beta[t][j];
		}
		for(int j=0; j<VOCAB; j++)
			m_gen->beta[t][j] = m_gen->beta[t][j]/temp;

	}




	// update for kappa_i

	for (int i=0; i<TOTAL_CLASSES; i++){
		m_gen->kappa[i] = 1.0/TOTAL_CLASSES;
	}
	//	cout << " NR for eta done! "<< endl;
	timestamp_t t2 = get_timestamp();
	double secinner = (t2 - t1) / 1000000.0L;
	cout<< " Time for M = "<< secinner<<endl;
	gsl_rng_free(r_generator);
}



vector<string >em_non_annotator_model_training(corpus* m_corpus, gen_model_params* m_gen,  
string src_folder/*="/home/divya/workspace/multiLabelCrowd/src/"*/, string output_file ){
	// lambda is observed here.
	double log_l=0;
	string model_filename;
	string logl_h_name = src_folder;
	boost::uuids::uuid uuid = boost::uuids::random_generator()();
	string folder_name = "model_non_ann/";
	folder_name.append(to_string(uuid));
	folder_name.append("/");
	string model_folder = src_folder; model_folder.append("model_non_ann/");
	mkdir(model_folder.c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH | S_IWOTH );
	string logl_h_folder = src_folder; logl_h_folder.append(folder_name);
	mkdir(logl_h_folder.c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH | S_IWOTH );
	fstream logl_h;
	string file_h = logl_h_folder; file_h.append("loglikelihood_non_ann_training.dat");
	logl_h.open(file_h.c_str(), fstream::out);
	cout<<" Created "<<logl_h_folder<< endl;
	double log_l_init = log_l;
	create_and_initialize_suff(TOTAL_CLASSES, TOTAL_TOPICS, VOCAB, TOTAL_ANN);
	for(int outer = 0; outer < MAX_EM_ITER ; ++outer)   //for entire EM
	{

		/*if ((outer%4 == 1) )
			continue;*/

		estep_non_annotator_model_training(m_corpus,  m_gen, outer);

		cout << " E"<< outer<< " done. " << endl;

		log_l =m_corpus->log_l;
		logl_h<< " E"<< outer<< ": "<< log_l<<endl;
		cout << " E"<< outer<< ": "<< log_l<<endl;

		mstep_non_annotator_model_training(m_corpus, m_gen); 
		cout << " M"<< outer<< " done. " << endl;
	
	

		if (outer%4 == 3){
			if ((abs((log_l -log_l_init)/log_l_init))<TOLERANCE)
			{
				string param_file_name = "param_";
				param_file_name.append(to_string(outer));
				string param_folder = logl_h_folder; param_file_name.append(".dump");
				m_gen->save_params_to_file(param_file_name.c_str(), param_folder);
				model_filename = param_folder+param_file_name;
				break;
			}
			else{
				cout<< " Relative diff = " << (abs((log_l -log_l_init)/log_l_init))<< endl;
				log_l_init = log_l;
			}
		}
		if (outer%2 == 0){
			string param_file_name = "param_";
			param_file_name.append(to_string(outer));
			string param_folder = logl_h_folder; param_file_name.append(".dump");
			m_gen->save_params_to_file(param_file_name.c_str(), param_folder);
			model_filename = param_folder+param_file_name;
		}

	}
	remove(output_file.c_str());
	m_gen->save_params_to_file(output_file, "./");
	logl_h.close();
	vector<string> model_folder_filename;
	model_folder_filename.push_back(logl_h_folder);
	model_folder_filename.push_back(model_filename);
	return model_folder_filename;
}

void run_non_annotator_model_train(string settings_file, string output_file){
	//		string settings_file="/home/divya/Satya-exec-multilabel/svmfilesbibtex/bibtex_settings.txt";
	//		if(argc==2)
	//			settings_file = argv[1];

	settings inp_settings = read_from_settings_file(settings_file);

	double *bins= new double[inp_settings.svm_vocab_size];
	int idRead = 0;
	string filepath=inp_settings.outer_src+ inp_settings.svm_delim_file;
	//cout<<filepath;
	fstream file_h;
	if(inp_settings.svm_DirectWords==0){
		cout<<"filepath" <<filepath<<endl;
		file_h.open(filepath.c_str(), fstream::in);
		if (!file_h.good()){
			cout  << " The file "<< filepath << " was not found! Please "
					"correct the settings file. "<< endl;
			exit(0);
		}
		while(!file_h.eof()&&idRead!=inp_settings.svm_vocab_size){
			string line; file_h >> line;
			bins[idRead]= stod(line);
			idRead++;
		}
		//for(int i=0; i<inp_settings.vocab_size; ++i)
		//	cout<<bins[i]<<endl;
		file_h.close();
		cout<<"end of bin reads" <<endl;
	}
	TOTAL_CLASSES = inp_settings.svm_classes;
	if(inp_settings.svm_topics==0)
		TOTAL_TOPICS = TOTAL_CLASSES ;
	else
		TOTAL_TOPICS = inp_settings.svm_topics;
	VOCAB = inp_settings.svm_vocab_size;
	corpus m_training_corpus;
	corpus m_test_corpus;
	
	filepath = inp_settings.outer_src+inp_settings.svm_trng_src;
	read_svm_file_to_get_corpus(&m_training_corpus, filepath, inp_settings,bins);
	cout<<m_training_corpus.all_docs.size()<<endl;
	filepath = inp_settings.outer_src+inp_settings.svm_test_src;
	read_svm_file_to_get_corpus(&m_test_corpus, filepath, inp_settings,bins);
	cout<<m_test_corpus.all_docs.size()<<endl;
	TOTAL_DOCS = m_training_corpus.all_docs.size();
	VIRTUAL_TOTAL_DOCS = TOTAL_DOCS;
	gen_model_params m_gen(TOTAL_CLASSES, TOTAL_TOPICS, VOCAB, TOTAL_ANN);
	cout<<TOTAL_TOPICS<<endl;


	///////Very Very important, create vector copies ///////
	for (map<short int, Doc*> ::iterator it = m_training_corpus.all_docs.begin(); it != m_training_corpus.all_docs.end();
			it++){
		m_training_corpus.all_docs_vec.push_back(it->second);
	}
	for (map<short int, Doc*> ::iterator it = m_test_corpus.all_docs.begin(); it != m_test_corpus.all_docs.end();
			it++){
		m_test_corpus.all_docs_vec.push_back(it->second);
	}
	///////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
	string src_folder = inp_settings.outer_src;




	vector<string> model_folderFilename = em_non_annotator_model_training(&m_training_corpus, &m_gen, src_folder, output_file);

	cout << " End of the training! 	The model is saved in final.model "<< endl;
	cout<<"End of code!"<<endl;
	delete []bins;
}

void run_non_annotator_model_test(string settings_file, string output_file){
	//		string settings_file="/home/divya/Satya-exec-multilabel/svmfilesbibtex/bibtex_settings.txt";
	//		if(argc==2)
	//			settings_file = argv[1];

	settings inp_settings = read_from_settings_file(settings_file);
	double *bins= new double[inp_settings.svm_vocab_size];
	int idRead = 0;
	string filepath=inp_settings.outer_src+ inp_settings.svm_delim_file;
	//cout<<filepath;
	fstream file_h;
	if(inp_settings.svm_DirectWords==0){
		cout<<"filepath" <<filepath<<endl;
		file_h.open(filepath.c_str(), fstream::in);
		if (!file_h.good()){
					cout  << " The file "<< filepath << " was not found! Please "
							"correct the settings file. "<< endl;
					exit(0);
		}
		while(!file_h.eof()&&idRead!=inp_settings.svm_vocab_size){
			string line; file_h >> line;
			bins[idRead]= stod(line);
			idRead++;
		}
		//for(int i=0; i<inp_settings.vocab_size; ++i)
		//	cout<<bins[i]<<endl;
		file_h.close();
		cout<<"end of bin reads" <<endl;
	}
	TOTAL_CLASSES = inp_settings.svm_classes;
	if(inp_settings.svm_topics==0)
		TOTAL_TOPICS = TOTAL_CLASSES ;
	else
		TOTAL_TOPICS = inp_settings.svm_topics;
	VOCAB = inp_settings.svm_vocab_size;
	corpus m_training_corpus;
	corpus m_test_corpus;
	
	
	filepath = inp_settings.outer_src+inp_settings.svm_trng_src;
	read_svm_file_to_get_corpus(&m_training_corpus, filepath, inp_settings,bins);
	cout<<m_training_corpus.all_docs.size()<<endl;
	filepath = inp_settings.outer_src+inp_settings.svm_test_src;
	read_svm_file_to_get_corpus(&m_test_corpus, filepath, inp_settings,bins);
	cout<<m_test_corpus.all_docs.size()<<endl;
	cout<<TOTAL_TOPICS<<endl;
	
	TOTAL_DOCS = m_training_corpus.all_docs.size();
	VIRTUAL_TOTAL_DOCS = TOTAL_DOCS;
	fstream output_file_h(output_file);
		if (!output_file_h.good())
		{
			cout <<" The file "<< output_file << " does not exist. Please "
					"train the model first by running ./model_annotator_train <settings_file.txt>"<<endl;
			exit(0);
		}
	gen_model_params* m_gen = read_param_file(output_file);

	///////Very Very important, create vector copies ///////
	for (map<short int, Doc*> ::iterator it = m_training_corpus.all_docs.begin(); it != m_training_corpus.all_docs.end();
			it++){
		m_training_corpus.all_docs_vec.push_back(it->second);
	}
	for (map<short int, Doc*> ::iterator it = m_test_corpus.all_docs.begin(); it != m_test_corpus.all_docs.end();
			it++){
		m_test_corpus.all_docs_vec.push_back(it->second);
	}
	///////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
	string src_folder = inp_settings.outer_src;
	
	int doc_count =0;
    //double logl_test = log_likelihood_test(m_test_corpus, &m_gen);
    string folder_to_be_created  =  inp_settings.outer_src+"test/";
    mkdir(folder_to_be_created.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH | S_IWOTH);
    //cout << " Test log likelihood = "<< logl_test<< endl;
    int total_num_docs = m_test_corpus.all_docs.size();
#pragma omp parallel for
	for (int doc_it = 0; doc_it <total_num_docs; doc_it++)
	{    
        Doc* curr_doc = m_test_corpus.all_docs_vec[doc_it];
        if (doc_count% 500 == 0)
            cout << " E-step "<< doc_count << " begins." << endl;

        estep_non_annotator_model_test(curr_doc, m_gen, folder_to_be_created);
        //curr_doc->m_var_model_doc->print_params();
        doc_count ++;
        //break;
    }
    //logl_test = log_likelihood_test(m_test_corpus, &m_gen, 1, src_folder);

    //cout << " Test log likelihood = "<< logl_test<< endl;
    
    vector<double> results = get_score(folder_to_be_created, inp_settings.outer_src ,inp_settings.output_file);
    cout << " Accuracy  = " << results[0]<<" Log-l = " << results[1] << " Micro-f1 =" << results[2] << endl;

	
	//cout << "Gen model params: " ;
	//m_gen.print_params();
	cout << " Finished test ! "<< endl;
	cout<<"End of code!"<<endl;
	if (bins!=NULL)
		delete []bins;
}

