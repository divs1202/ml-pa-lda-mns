/*
 * utils.cpp
 *
 *  Created on: 14-Feb-2016
 *      Author: divya
 */

#include "utils.h"

int check_non_alphanum(string word) {
	for (int iter = 0; iter < word.size(); iter++)
		if (word[iter] < 'a' || word[iter] > 'z')
			return 1;
	return 0;
}

bool vocab_value_compare(pair<string, int> a, pair<string, int> b) {

	if (b.second == a.second)
		return b.first < a.first;
	else
		return b.second < a.second;
}
vector<string> ls_command(string path){
	vector<string> result;
		dirent* de;
		DIR* dp;
	dp = opendir(path.c_str());
		if (dp) {
			while (true) {
				de = readdir(dp);
				if (de == NULL)
					break;
				//cout<< de->d_name<<endl;
				result.push_back(std::string(de->d_name));
			}
			closedir(dp);
			std::sort(result.begin(), result.end());
		}
		return result;
}
vector<string> read_directory(string path) {
	vector<string> result;
	dirent* de;
	DIR* dp;

	dp = opendir(path.c_str());
	if (dp) {
		while (true) {
			de = readdir(dp);
			if (de == NULL)
				break;
			if (!strstr(de->d_name, ".txt"))
				continue;
			//cout<< de->d_name<<endl;
			result.push_back(std::string(de->d_name));
		}
		closedir(dp);
		std::sort(result.begin(), result.end());
	}
	return result;
}

set<string> populate_word_list(string filename) {
	fstream file_h(filename.c_str());
	string line;
	set<string> wordlist;
	while (!file_h.eof()) {
		file_h >> line;
		wordlist.insert(line);
	}
	return wordlist;
}

int phase1_processing(string src_dir) {
	/*
	 * Process the files created from the raw xml and generates new files with stop words removed.
	 */
	string phase1_dir = src_dir; phase1_dir.append("phase1/");
	string stopword_dir = src_dir; stopword_dir.append("stopwords.txt");

	vector<string> filelist = read_directory(phase1_dir);
		//	"/home/divya/workspace/multiLabelCrowd/src/phase1/");
	set<string> stopwordlist = populate_word_list(stopword_dir);
		//	"/home/divya/workspace/multiLabelCrowd/src/stopwords.txt");

	int counter = 0;
	map<string, int> vocab;

	fstream file_h;
	fstream output_file_h;
	for (int list_it = 0; list_it < filelist.size(); list_it++) {
		string dirname = phase1_dir;//"/home/divya/workspace/multiLabelCrowd/src/phase1/";
		string output_dirname = src_dir;
		output_dirname.append("phase2/");//		"/home/divya/workspace/multiLabelCrowd/src/phase2/";
		string file_name = filelist[list_it];
		string filepath = dirname.append(file_name);
		string output_filepath = output_dirname.append(file_name);
		//cout<<filepath<<endl;
		int doc_start = 0;
		output_file_h.open(output_filepath.c_str(), fstream::out);
		file_h.open(filepath.c_str(),
				fstream::in | fstream::out | fstream::app);
		while (!file_h.eof()) {
			string line;
			file_h >> line;
			counter++;
			if (doc_start == 0)
				output_file_h << line << "\n";

			if (check_non_alphanum(line) != 0 && (doc_start == 0)) {
				doc_start = 1;
				continue;
			}
			if (check_non_alphanum(line) != 0)
				continue;
			if (stopwordlist.find(line) != stopwordlist.end())
				continue;
			if (doc_start == 1) {
				vocab[line]++;
				output_file_h << line << " ";
				//cout<< counter<<". "<<line<<" " << check_non_alphanum(line)<< endl;
			}

		}
		file_h.close();
		output_file_h.close();
	} //PROCESS ALL FILES
	std::vector<std::pair<string, int> > vocab_sort;
	vocab_sort.insert(vocab_sort.begin(), vocab.begin(), vocab.end());
	sort(vocab_sort.begin(), vocab_sort.end(), vocab_value_compare);
	cout << vocab_sort.size() << " words inserted." << endl;
	int count = 0;
	/*for (vector<pair<string,int> >::iterator it=vocab_sort.begin(); it!=vocab_sort.end(); ++it , ++count)
	 std::cout <<  count<<". "<< it->first << " => " << it->second << endl;

	 for (set<string>::iterator it=stopwordlist.begin(); it!=stopwordlist.end(); ++it)
	 cout <<  *it << endl;*/
	return 0;
}

map<string, int> form_vocab_count(string dir_arg) {
	//dir_arg = "/home/divya/workspace/multiLabelCrowd/src/phase3/"
	map<string, int> vocab;
	vector<string> filelist = read_directory(dir_arg);
	fstream file_h;
	for (int filecount = 0; filecount < filelist.size(); filecount++) {
		int doc_start = 0;
		string dirname = dir_arg;
		string file_name = filelist[filecount];
		string filepath = dirname.append(file_name);
		file_h.open(filepath.c_str(), fstream::in);
		while (!file_h.eof()) {
			string line;
			file_h >> line;
			if (strcmp(line.c_str(), "*****") == 0) {
				doc_start = 1;
				continue;
			}
			if (doc_start == 1)
				vocab[line]++;
		}
		file_h.close();
	}
	cout << " Size of vocab = " << vocab.size() << endl;
	std::vector<std::pair<string, int> > vocab_sort;
	vocab_sort.insert(vocab_sort.begin(), vocab.begin(), vocab.end());
	sort(vocab_sort.begin(), vocab_sort.end(), vocab_value_compare);
	int count = 0;

	// Process each document once again.
	return vocab;
}

int phase3_processing(string src_dir) {
	string phase3_dir = src_dir; phase3_dir.append("phase3/");
	map<string, int> vocab = form_vocab_count( phase3_dir);
			//"/home/divya/workspace/multiLabelCrowd/src/phase3/");
	set<string> usedwords;
	int count = 0;
	/*std::vector<std::pair<string,int> > vocab_sort;
	 vocab_sort.insert(vocab_sort.begin(), vocab.begin(), vocab.end());
	 sort(vocab_sort.begin(), vocab_sort.end(), vocab_value_compare );*/

	vector<string> filelist = read_directory(phase3_dir);
		//	"/home/divya/workspace/multiLabelCrowd/src/phase3/");
	fstream file_h, output_file_h;
	for (int filecount = 0; filecount < filelist.size(); filecount++) {
		int doc_start = 0;
		string dirname = phase3_dir;// "/home/divya/workspace/multiLabelCrowd/src/phase3/";
		string output_dir = src_dir; //"/home/divya/workspace/multiLabelCrowd/src/phase4/";
		output_dir.append("phase4/");
		string file_name = filelist[filecount];
		string filepath = dirname.append(file_name);
		string output_filepath = output_dir.append(file_name);
		file_h.open(filepath.c_str(), fstream::in);
		output_file_h.open(output_filepath.c_str(), fstream::out);
		while (!file_h.eof()) {
			string line;
			file_h >> line;

			if (doc_start == 0)
				output_file_h << line << endl;
			if (strcmp(line.c_str(), "*****") == 0) {
				doc_start = 1;
				continue;
			}

			if (doc_start == 1) {
				if (vocab[line] >= 50) {
					output_file_h << line << " ";
					usedwords.insert(line);
				}
			}
		}
		file_h.close();
		output_file_h.close();

	}

	cout << "Completed Phase 4!" << endl;
	fstream vocab_file, vocab_with_numbers;
	vocab_file.open("/home/divya/workspace/multiLabelCrowd/src/vocab_list_phase4.txt",
			fstream::out);
	vocab_with_numbers.open(
			"/home/divya/workspace/multiLabelCrowd/src/vocab_list_with_count_phase4.txt",
			fstream::out);

	for (set<string>::iterator it = usedwords.begin(); it != usedwords.end();
			++it) {
		cout << *it << endl;
		vocab_with_numbers << *it << "\t" << vocab[*it] << endl;
		vocab_file << *it << endl;
	}
	vocab_with_numbers.close();
	vocab_file.close();
	return 0;
}

int phase4_processing() {
	map<string, int> topic_list;
	fstream files_copied;
	files_copied.open(
			"/home/divya/workspace/multiLabelCrowd/src/docs_copied_to_phase5.txt",
			fstream::out);
	int num_files_copied = 0;
	vector<string> filelist = read_directory(
			"/home/divya/workspace/multiLabelCrowd/src/phase4/");
	fstream file_h;
	for (int filecount = 0; filecount < filelist.size(); filecount++) {
		int doc_start = 0;
		string dirname = "/home/divya/workspace/multiLabelCrowd/src/phase4/";
		string file_name = filelist[filecount];
		string filepath = dirname.append(file_name);
		file_h.open(filepath.c_str(), fstream::in);
		int word_count = 0;
		set<string> curr_topic_list;
		while (!file_h.eof()) {
			string line;
			file_h >> line;

			if (strcmp(line.c_str(), "*****") == 0) {
				doc_start = 1;
				continue;
			}
			if (doc_start == 0)
				curr_topic_list.insert(line);
			if (doc_start == 1) {
				word_count++;
				//cout<< word_count << ". "<< line<< endl;
			}

		}
		file_h.close();
		if (word_count > 20) {
			ifstream inFile(filepath.c_str());
			string out_dirname =
					"/home/divya/workspace/multiLabelCrowd/src/phase5/";
			string out_file_path = out_dirname.append(file_name);
			ofstream outFile(out_file_path.c_str());
			outFile << inFile.rdbuf();
			inFile.close();
			outFile.close();
			num_files_copied++;
			for (set<string>::iterator it = curr_topic_list.begin();
					it != curr_topic_list.end(); ++it)
				topic_list[*it]++;
			files_copied << file_name << endl;
		}
	}

	fstream topic_file, topic_with_numbers;
	topic_file.open("/home/divya/workspace/multiLabelCrowd/src/class_list.txt",
			fstream::out);
	topic_with_numbers.open(
			"/home/divya/workspace/multiLabelCrowd/src/class_list_with_count.txt",
			fstream::out);

	for (map<string, int>::iterator it = topic_list.begin();
			it != topic_list.end(); ++it) {

		topic_with_numbers << it->first << "\t" << it->second << endl;
		topic_file << it->first << endl;
	}
	files_copied.close();
	topic_with_numbers.close();
	topic_file.close();
	cout << " No.of files copied = " << num_files_copied;
	return 0;
}

map<string,int> fetch_topic_list(string dir, int n_value){
	// Fetch top n_value topics from the files in dir.
	vector<string> filelist = read_directory(dir);//"/home/divya/workspace/multiLabelCrowd/src/phase5/");
	fstream file_h;
	map<string, int> topic_list;
	cout<< " no. of files = "<< filelist.size()<<endl;
	for (int filecount = 0; filecount < filelist.size(); filecount++) {
		int doc_start = 0;
		string dirname = dir;
		string file_name = filelist[filecount];
		string filepath = dirname.append(file_name);
		file_h.open(filepath.c_str(), fstream::in);
		int word_count = 0;
		set<string> curr_topic_list;
		while (!file_h.eof()) {
			string line;
			file_h >> line;

			if (strcmp(line.c_str(), "*****") == 0) {
				doc_start = 1;
				continue;
			}
			if (doc_start == 0)
			{
				topic_list[line]++;
				curr_topic_list.insert(line);
			}
			if (doc_start == 1) {
				word_count++;
				//cout<< word_count << ". "<< line<< endl;
			}

		}
		file_h.close();
		if (curr_topic_list.empty())
			cout<<" Empty classes : "<< file_name<<endl;
	}

	int count = 0;
	std::vector<std::pair<string, int> > topic_sort;
	topic_sort.insert(topic_sort.begin(), topic_list.begin(), topic_list.end());
	sort(topic_sort.begin(), topic_sort.end(), vocab_value_compare);
	int num_topics_to_be_fetched = n_value;
	if (n_value>topic_sort.size())
		num_topics_to_be_fetched = topic_sort.size();
	vector<pair<string, int> > top_n (&topic_sort[0],&topic_sort[num_topics_to_be_fetched]);
	map<string, int> top_n_topics_map;
	for (vector<pair<string, int> >::iterator it = top_n.begin();
			it != top_n.end(); ++it, ++count)
	{
		top_n_topics_map[it->first] = it->second;
		cout << count << ". " << it->first << " => " << it->second << endl;
	}
	return top_n_topics_map;
}
void phase5_processing() {

	/* Prepare new set of documents with top 10 topics alone. */
	string phase5_dir = "/home/divya/workspace/multiLabelCrowd/src/phase5/";
	string phase6_dir = "/home/divya/workspace/multiLabelCrowd/src/phase6_without_wheat/";
	int num_topics = 9;
	map<string,int> top_n_topics_map = fetch_topic_list(phase5_dir, num_topics);
	vector<string> filelist = read_directory(phase5_dir);
	fstream file_h; int num_files_copied = 0;
	fstream files_copied;
	files_copied.open("/home/divya/workspace/multiLabelCrowd/src/docs_copied_to_phase6.txt",
			fstream::out); int max_word_length = 0; string longest_filename;
	for (int filecount = 0; filecount < filelist.size(); filecount++) {
		int doc_start = 0;
		string dirname = phase5_dir;
		string file_name = filelist[filecount];
		string filepath = dirname.append(file_name);
		file_h.open(filepath.c_str(), fstream::in);
		int word_count = 0;
		set<string> curr_topic_list; bool discard_doc = false;
		while (!file_h.eof()) {
			string line;
			file_h >> line;

			if (strcmp(line.c_str(), "*****") == 0) {
				doc_start = 1;
				continue;
			}
			if (doc_start == 0)
			{
				if (top_n_topics_map.find(line) == top_n_topics_map.end())
					discard_doc = true;
				continue;
			}
			if (doc_start == 1)
				word_count++;

		}
		file_h.close();
		if (discard_doc == false){
		ifstream inFile(filepath.c_str());
		string out_dirname = phase6_dir;
		string out_file_path = out_dirname.append(file_name);
		ofstream outFile(out_file_path.c_str());
		outFile << inFile.rdbuf();
		inFile.close();
		outFile.close();
		num_files_copied++;
		if (word_count > max_word_length){
			max_word_length = word_count;
			longest_filename = file_name;
		}

		files_copied << file_name << " \t " << word_count<<endl;
		}
	}
	cout << " Longest doc size = "<< max_word_length<<" , "<< longest_filename;
	files_copied.close();

	write_final_topic_to_file(phase6_dir, num_topics+ 20);
	write_final_vocab_to_file(phase6_dir);
}

void write_final_vocab_to_file(string dir){
	fstream final_vocab, final_vocab_with_numbers;
		 final_vocab.open("/home/divya/workspace/multiLabelCrowd/src/final_vocab_without_wheat.txt", fstream::out);
		 final_vocab_with_numbers.open("/home/divya/workspace/multiLabelCrowd/src/final_vocab_with_numbers.txt", fstream::out);
		 map<string, int> vocab = form_vocab_count(dir);//"/home/divya/workspace/multiLabelCrowd/src/phase6/");
		 cout <<" Total vocab size = "<< vocab.size()<<endl;
		 cout<<" Printing vocab:";
		 for (map<string,int>::iterator it=vocab.begin(); it!=vocab.end(); ++it){
		 cout<<it->first<<" \t " << it->second<<endl;
		 final_vocab<< it->first<< endl;
		 final_vocab_with_numbers<< it->first<<" \t " << it->second<<endl;
		 }
		 cout<< " Done!";
		 final_vocab.close();
		 final_vocab_with_numbers.close();
}
template <typename T1,typename T2>
void print_map(map<T1, T2> arg){
	int count  = 1;
	for (typename map<T1, T2>::iterator it =  arg.begin(); it != arg.end(); it++, ++count)
		cout << count<<". "<< it->first<<" -> "<< it->second<<endl;
	return;

}

void write_final_topic_to_file(string dirname, int max_topics){
	cout << " Writing topic to file" << endl;
	map<string,int> topic_list = fetch_topic_list(dirname, max_topics);
	fstream final_topic, final_topic_with_numbers;
	final_topic.open("/home/divya/workspace/multiLabelCrowd/src/final_class_without_wheat.txt", fstream::out);
	final_topic_with_numbers.open("/home/divya/workspace/multiLabelCrowd/src/final_class_with_numbers.txt", fstream::out);
	for (map<string, int>::iterator it =  topic_list.begin(); it != topic_list.end(); it++)
	{
		final_topic_with_numbers << it->first<<" \t "<< it->second<<endl;
		final_topic << it->first<<endl;
		cout<< it->first<<" \t "<< it->second<<endl;
	}
	final_topic.close();
	final_topic_with_numbers.close();
	return;
}

void form_splits_to_training_and_test(string input_dir, double training_percent){
	vector<string> file_list = read_directory(input_dir);
	int total_num_files = file_list.size();
	std::random_shuffle ( file_list.begin(), file_list.end() );
	int tr_set_size = training_percent*total_num_files;
	cout << tr_set_size << " training documents ";
	string trng_set_dir = "/home/divya/workspace/multiLabelCrowd/src/trng_set_without_wheat/";
	string test_set_dir = "/home/divya/workspace/multiLabelCrowd/src/test_set_without_wheat/";
	mkdir(trng_set_dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH | S_IWOTH);
	mkdir(test_set_dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH | S_IWOTH);

	for (int i=0; i< tr_set_size; i++){
		string filepath =input_dir;
		filepath.append(file_list[i]);
		ifstream inFile(filepath.c_str());
		string out_dirname = trng_set_dir;
		string out_file_path = out_dirname.append(file_list[i]);
		ofstream outFile(out_file_path.c_str());
		outFile << inFile.rdbuf();
		inFile.close();
		outFile.close();
	}
	for (int i=tr_set_size; i< total_num_files; i++){
		string filepath = input_dir;
		filepath.append(file_list[i]);
		ifstream inFile(filepath.c_str());
		string out_dirname = test_set_dir;
		string out_file_path = out_dirname.append(file_list[i]);
		ofstream outFile(out_file_path.c_str());
		outFile << inFile.rdbuf();
		inFile.close();
		outFile.close();
	}
}

/*
int main() {
	//map<string,int> topic_list = fetch_topic_list("/home/divya/workspace/multiLabelCrowd/src/phase3/", 1000);
	//write_final_topic_to_file("/home/divya/workspace/multiLabelCrowd/src/phase6/", 15);
	phase5_processing();
	//map<string, int> vocab_phase2= form_vocab_count( "/home/divya/workspace/multiLabelCrowd/src/phase2/") ;
	cout << " Done";
	return 0;
}
*/
