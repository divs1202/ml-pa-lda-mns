/*
 * gen_model.h
 *
 *  Created on: 14-feb-2016
 *      Author: divya
 */
#ifndef GEN_MODEL_H_
#define GEN_MODEL_H_

#include "utils.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_sf_psi.h>
#include <math.h>
#include <string.h>
#include <sstream>
#include <sys/time.h>
#include <sys/stat.h>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>

class gen_model_params{
public:
	double *xi;//[TOTAL_CLASSES];
	double ***alpha;//[TOTAL_CLASSES][2][TOTAL_TOPICS];
	double **beta;//[TOTAL_TOPICS][VOCAB];
	double *rho;//[TOTAL_ANN];
	double *kappa;//[TOTAL_CLASSES];
	int m_total_classes,m_total_topics, m_total_ann, m_vocab;
	gen_model_params(int total_classes, int total_topics, int vocab, int total_ann)
	{
		long seed = time(NULL);
		gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);
		gsl_rng_set(r, seed);
		m_total_classes = total_classes; m_total_topics = total_topics;
		m_total_ann = total_ann; m_vocab = vocab;
		xi = new double[total_classes];
		alpha = new double**[total_classes];
		for(int i=0; i<total_classes; ++i)
		{
			xi[i] = gsl_ran_beta(r, 10, 10);
			alpha[i] = new double*[2];
			alpha[i][0] = new double[total_topics];
			alpha[i][1] = new double[total_topics];
			for(int t=0; t< total_topics; t++){
				alpha[i][0][t] =gsl_ran_beta(r, 1,1);
				alpha[i][1][t] =gsl_ran_beta(r, 1,1);
				/*alpha[i][0][t] = 10;
            	alpha[i][1][t] = 10;*/
			}
		}
		beta = new double*[total_topics];
		double *tempdir = new double[vocab];
		for(int i=0; i<total_topics; ++i)
		{
			beta[i] = new double[vocab];
			for(int j=0; j<vocab; j++)
			{
				tempdir[j]=0.1;
				//eta[i][j] = gsl_ran_beta(r, 1, 1);
			}
			gsl_ran_dirichlet(r,vocab,tempdir,beta[i]);
			//beta[i][j] = 1.0/vocab;
		}
		delete [] tempdir;

		rho = new double[total_ann];
		for(int i=0; i<total_ann; i++)
		{
			rho[i] = gsl_ran_beta(r, 1,1);
			if (rho[i]<0.5)
				rho[i] = 1 - rho[i];
		}

		kappa = new double[total_classes];
		//double alph_temp[m_total_classes];
		for(int i=0; i<m_total_classes; i++)
		{
			//alph_temp[i]=10;
			kappa[i] = 1.0/m_total_classes;
		}
		//   gsl_ran_dirichlet(r,total_classes,alph_temp,kappa);
		gsl_rng_free (r);
	}
	void print_params(){
		cout << "\n xi: ";
		for (int i=0; i< m_total_classes; i++)
			cout<<xi[i]<<" ";
		cout << "\n rho: ";
		for (int i=0; i< m_total_ann; i++)
			cout<<rho[i]<<" ";
		cout << "\n alpha: ";
		for (int i=0; i<m_total_classes; i++){
			for (int j=0; j<=1; j++){
				for (int t=0; t<m_total_topics; t++){
					cout<<alpha[i][j][t]<<" ";;
				}
				cout<<endl;
			}
			cout<<endl;
		}
		cout << "\n beta: ";
		for (int t=0; t<m_total_topics; t++){
			for (int j=0; j<m_vocab; j++){

				cout<<beta[t][j]<<" ";;


			}
			cout<<endl;
		}
		cout << "\n kappa: ";
		for (int i=0; i< m_total_classes; i++)
			cout<<kappa[i]<<" ";
	}

	void save_params_to_file(string file_name,
			string src_folder = "/home/divya/workspace/multiLabelCrowd/src/model/" ){
		string file_path = src_folder;
		file_path.append(file_name.c_str());
		fstream file_h;
		file_h.open(file_path.c_str(), fstream::out);
		file_h << " xi: "<< endl;
		for (int i=0; i< m_total_classes; i++)
			file_h << xi[i]<<" ";
		file_h << "\n rho: "<< endl;
		for (int i=0; i< m_total_ann; i++){
			file_h << rho[i]<<" ";
		}
		file_h << "\n kappa: "<< endl;
		for (int i=0; i< m_total_classes; i++){
			file_h << kappa[i]<<" ";
		}

		file_h<< " \n alpha: "<< endl;
		for (int i=0; i< m_total_classes; i++){
			for (int j=0 ; j<=1; j++){
				for (int t =0; t< m_total_topics; t++){
					file_h<<alpha[i][j][t]<<" ";
				}
				file_h<< endl;
			}
			file_h<< endl;
		}
		file_h<<" \n beta: " << endl;
		for (int t=0; t<m_total_topics; t++){
			for (int j=0; j<m_vocab; j++){
				file_h<< beta[t][j]<<" ";
			}
			file_h<<endl;
		}
		file_h.close();
	}
	~gen_model_params()    {
		delete[] xi;
		for(int i=0; i<m_total_classes; ++i)
		{
			delete[] alpha[i][0];
			delete[] alpha[i][1];
			delete[] alpha[i];
		}
		delete[] alpha;

		for(int i=0; i<m_total_topics; ++i)
			delete[] beta[i];

		delete[] beta;

		delete[] rho;
		delete[] kappa;
	}
};

#endif
