#include "model_non_annotator_utils.h"
int main(int argc, char* argv[])
{
	string settings_file="dsdesc_reuters.txt";
	string output_file="final.model";
	if(argc==2)
		settings_file = argv[1];
	run_non_annotator_model_test(settings_file,output_file);
}
