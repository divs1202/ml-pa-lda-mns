****************************************************************************
Multilabel Presence Absence Latent Dirichlet Allocation Crowd (ML-PA-LDA-MNS)
****************************************************************************

Divya Padmanabhan
divya.padmanabhan[at]csa.iisc.ernet.in

Satyanath Bhat
satya.bhat[at]gmail.com

------------------------------------------------------------------------

Prerequisites

We have tested this code extensively with the following datasets. We ran
this in ubuntu 14.04 64 bit linux distribution. The following is a non-
exhaustive list of packages required to run this code

a) boost c++ libraries
b) openmp libraries
c) standard template libraries
d) libxml
e) GNU scientific libraries (GSL)

------------------------------------------------------------------------

Abstract

Multi-label classification is a supervised machine learning  problem 
where an instance is associated with multiple classes. The main challenge 
is learning the correlations between the classes.  A further dimension of 
challenge occurs when the labels of training instances are procured from 
heterogenous noisy crowdworkers with unknown qualities.  We first assume 
labels from a perfect source and propose a topic model where the present 
as well as the absent classes generate the latent topics and thereby the 
words. We then adapt our topic model to the scenario where the labels are 
provided by noisy crowdworkers.  By carrying out several experiments on 
real world datasets, we show that our model achieves excellent 
performance.  Our model also learns the qualities of the annotators, even 
with minimal training data.

The current code repo is an implementation of the suggested approach.

----------------------------------------------------------------------- 

TABLE OF CONTENTS


1. COMPILING

2. EXECUTION 

   A. DATA FILE FORMAT
   
   B. SETTINGS FILE

------------------------------------------------------------------------

1. COMPILING

Type "make" in a shell.
This generates four executables
a) model_non_annotator_train
b) model_non_annotator_test
c) model_annotator_train
d) model_annotator_test


------------------------------------------------------------------------

2. EXECUTION
If you pull from this repository, you should be able to directly 
work with the reuters dataset which is part of this release.

Train ML-PA-LDA Non annotator model with reuters train dataset:
--------------------------------------------------------------
./model_non_annotator_train dsdesc_reuters.txt

Test ML-PA-LDA Non annotator model with reuters test dataset:
--------------------------------------------------------------
./model_non_annotator_test dsdesc_reuters.txt

Train ML-PA-LDA annotator model with reuters train dataset:
--------------------------------------------------------------
./model_annotator_train dsdesc_reuters.txt

Train ML-PA-LDA annotator model with reuters test dataset:
--------------------------------------------------------------
./model_annotator_test dsdesc_reuters.txt


A. DATA FILE FORMAT
The data file format which the code accepts is svm format.
https://www.csie.ntu.edu.tw/~cjlin/libsvm/. The libsvm site
also provides several of the datafiles used in our input. 

Caution!!: To run svm file inputs with real features, the
features need to be converted to words using the technique
described in paper. We used a simple python script for the same

B. SETTINGS FILE

The following is the content of the sample data set description
settings file (dsdesc_reuters.txt)

--------------------------------------|
num_ann		50                        |
output_file	out_file.txt	          |
outer_src	./datasets/Reuters21578/  |
svm_trng train.svm                    |
svm_test test.svm                     |
svm_vocab 1996                        |
svm_classes 10                        |
svm_direct_words 1                    |
svm_split invalid                     |
svm_topics 20                         |
--------------------------------------|

num_ann: Number of annotators simulated. 20% of these are 
malicious( success rate <0.1). 30% low (between 0.51 and 0.65)
40% medium (between 0.66 and 0.85) and rest 10% are high quality
(>0.86)

output_file: A verbose summary is put in this file

outer_src: Directory containing the input dataset

svm_trng Training dataset in svm format
                    
svm_test Test dataset in svm format        
             
svm_vocab number of words in the vocabulary    

svm_classes Total number of classes

svm_direct_words: 1 indicates that the dataset is already is 
format suitable for the model directly. 0 indicates that the 
dataset has real features. This requires creation of a csv format
split file which contains the cluster centers

svm_split: The cluster center files in non-decreasing order.
When the dataset is read, this file governs how the real features 
are converted to words.

svm_topics: Total number of topics to be used for training/test.